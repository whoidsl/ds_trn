BEGIN {
    FS = " ";
    nLines = 1;
    prev_line = "";
    preceding_comments = 0;
    is_regular = 1;
	fname=""
}
function basename(file) {
sub(".*/", "", file)
return file
}

{
#if( !match($0,"^[ \t]*typedef") && !match($0,"^[ \t]*#define") ){
#gsub("[ \t]*MLocal "," static ");
#gsub("[ \t]*Int64" ," int64_t ");
#gsub("[ \t]*Int32" ," int32_t ");
#gsub("[ \t]*Int16" ," int16_t ");
#gsub("[ \t]*Int8" ," int8_t ");
#gsub("[ \t]*Nat32" ," uint32_t ");
#gsub("[ \t]*Nat16" ," uint16_t ");
#gsub("[ \t]*Nat64" ," uint64_t ");
#gsub("[ \t]*Flt32" ," float ");
#gsub("[ \t]*Flt64" ," double ");
#gsub("[ \t]*MBool" ," bool ");
#gsub("[ \t]*Word" ," unsigned short ");
#gsub("[ \t]*DWord" ," unsigned long ");
#gsub("[ \t]*BitVec" ," uint32 ");
#gsub("[ \t]*Void" ," void ");
#gsub("[ \t]*Char" ," unsigned char ");
#gsub("[ \t]*Byte" ," uint8_t ");
#gsub("[ \t]*Errno" ," int ");
#gsub("[ \t]*Extern" ,"  extern ");
#}

	if( nLines == 1 )
	{
		print "/// @file " basename(FILENAME)
		nLines++
	}

    is_regular=1;
    
# the only case that matters is
# when a non-blank line is directly
# preceded by a C comment closure


   if( match($0,"[*][/][ \t]*[\x0A\x0D]*$") )
    {
# C comment end
        preceding_comments = preceding_comments + 1;
		is_regular=0;
    }
    else
    if( match($0,"^[ \t]*[ \t][\x0D]*$") || (length($0) == 0) )
    {
# blank line
        is_regular=0;
    }
    else
    if( match($0,"^[ \t]*[/]{2,3}[ ]{1,2}[@]") )
    {
# doxygen comment
    is_regular=0;
	preceding_comments = 0;
    }
    else
    if( match($0,"^[ \t]*[/]{2,}") )
    {
# other comment
    is_regular=0;
    }

    if( ( is_regular == 1 ) && ( preceding_comments > 0 ) ){
        msg = ""
		print msg
        preceding_comments = 0;
    }

# replace "} /* comment */" with "}"
gsub("[ \t]*[}][ \t]*[/][*].*[*][/]","}");
gsub("[/][*]","//");
gsub("[*][/]","//");

    print
#    print $0"  ["is_regular preceding_comments"]"
}

END {
}
