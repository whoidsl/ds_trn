//-------------------------------------------------------------------------
//
// rewritersample.cpp: Source-to-source transformation sample with Clang,
// using Rewriter - the code rewriting interface.
//
// This rewriter inserts Doxygen comment stubs into source code
// and prints to stdout.
//
// copyright 2015 MBARI
// @author k headley
// original example by Eli Bendersky (eliben@gmail.com)
// This code is in the public domain
//

#include <cstdio>
#include <string>
#include <sstream>
#include <libgen.h>
#include <iostream>
#include <fstream>
#include <unistd.h>

#include "clang/AST/ASTConsumer.h"
#include "clang/AST/ASTContext.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/AST/RawCommentList.h"
#include "clang/Basic/Diagnostic.h"
#include "clang/Basic/FileManager.h"
#include "clang/Basic/SourceManager.h"
#include "clang/Basic/TargetOptions.h"
#include "clang/Basic/TargetInfo.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Lex/Preprocessor.h"
#include "clang/Parse/ParseAST.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Rewrite/Frontend/Rewriters.h"
#include "clang/Lex/PreprocessingRecord.h"
#include "llvm/Support/Host.h"
#include "llvm/Support/raw_ostream.h"

using namespace clang;
using namespace std;


// By implementing RecursiveASTVisitor, we can specify which AST nodes
// we're interested in by overriding relevant methods.
class MyASTVisitor : public RecursiveASTVisitor<MyASTVisitor>
{
public:
    MyASTVisitor(Rewriter &R)
    : TheRewriter(R)
    {}
	
    template <class C>
    bool hasDocComment(C* decl){
        
        bool retval=false;
        
        ASTContext& ctx = decl->getASTContext();
        SourceManager& sm = ctx.getSourceManager();
        RawCommentList &rclist = ctx.getRawCommentList();
        ArrayRef<RawComment *> clist = rclist.getComments();
        llvm::ArrayRef<RawComment *>::iterator it;

        RawComment *rc= NULL;
        // iterate over the source manager's raw comments
        // note that the list spans all of the source
        // files, so a comment may not be in the same file
        // as the decl passed in.
        for (unsigned i=0; i<clist.size(); i++) {
            
            rc= clist[i];

            stringstream SSBefore;
//  		cerr<<"MyASTVisitor::hasDocComment - Got here!"<<"\n";
            if (rc!=NULL) {
                std::string raw       = rc->getRawText(sm);
                //std::string brief     = rc->getBriefText(ctx);
                SourceRange c_range   = rc->getSourceRange();
                SourceRange d_range   = decl->getSourceRange();
                PresumedLoc cpl_start = sm.getPresumedLoc(c_range.getBegin());
                PresumedLoc cpl_end   = sm.getPresumedLoc(c_range.getEnd());
                PresumedLoc dpl_start = sm.getPresumedLoc(d_range.getBegin());
                PresumedLoc dpl_end   = sm.getPresumedLoc(d_range.getEnd());

//                SSBefore<<"RAW_COMMENT:["<<raw<<"]\n";
//                SSBefore<<"C_LOC:["<<cpl_start.getLine()<<":" \
//                <<cpl_start.getColumn()<<","<<cpl_end.getLine()<<":" \
//                <<cpl_end.getColumn()<<"]\n";
//                SSBefore<<"D_LOC:["<<dpl_start.getLine()<<":" \
//                <<dpl_start.getColumn()<<","<<dpl_end.getLine()<<":" \
//                <<dpl_end.getColumn()<<"]\n";
                
                // return TRUE if comment is:
                // - comment is in same file as decl AND
                // - it's a doxygen comment AND
                // - it's directly adjacent to the declaration
                if ( dpl_start.getFilename()==cpl_start.getFilename() &&
					(rc->getKind()==clang::RawComment::RCK_BCPLSlash ||
                      rc->getKind()==clang::RawComment::RCK_BCPLExcl ||
                      rc->getKind()==clang::RawComment::RCK_JavaDoc ||
                      rc->getKind()==clang::RawComment::RCK_Qt
                      ) &&
                    cpl_end.getLine()<dpl_start.getLine() &&
                    cpl_end.getLine()>=dpl_start.getLine()-1) {
                    retval=true;
                    
                    cerr<<"raw comment - type: "<<rc->getKind()<<" "
                    <<"C["<<cpl_start.getFilename()<<" l"<<cpl_start.getLine()<<":"<<cpl_end.getLine()<<"] " \
                    <<"D["<<dpl_start.getFilename()<<" l"<<dpl_start.getLine()<<":"<<dpl_end.getLine()<<"] text:\n" \
                    << raw \
                    <<"\n";
                    break;
//                SSBefore<<"RAW_COMMENT:["<<raw<<"]\n";
//                SSBefore<<"[EXISTING DECL COMMENT]\n";
                }
            }
        }
        return retval;
    }
    
    // document class declarations
    bool VisitRecordDecl(RecordDecl *decl) {
        if (decl->isClass()) {
            if (!hasDocComment(decl)) {
                stringstream SSBefore;
                SSBefore << "/// @class " << decl->getNameAsString() << "\n";
                SSBefore << "/// @brief TBD" << "\n";
                SourceLocation ST = decl->getSourceRange().getBegin();
                TheRewriter.InsertText(ST, SSBefore.str(), true, true);
            }
        }
        return true;
    }

    // document typedef declarations
    bool VisitTypedefDecl(TypedefDecl *decl) {
        stringstream SSBefore;
        
        if (!hasDocComment(decl)) {
            QualType QT=decl->getUnderlyingType();
//            if (QT.getAsString().find("anonymous")>0) {
//                SSBefore << "/// @typedef " << decl->getNameAsString()<<"_s"<<" "<< decl->getNameAsString() << "\n";
//            }else{
//                SSBefore << "/// @typedef " << QT.getAsString()<<" "<< decl->getNameAsString() << "\n";
//            }
            SSBefore << "/// @typedef " << QT.getAsString()<<" "<< decl->getNameAsString() << "\n";
            SSBefore << "/// @brief TBD" << "\n";
    
            // create a comment for the underlying type as well
            const Type *DT=decl->getTypeForDecl();
            if (DT && DT->isAggregateType()) {
                SSBefore << "/// @" << QT.getAsString()<<"\n";
                SSBefore << "/// @brief TBD" << "\n";
            }
            SourceLocation ST = decl->getSourceRange().getBegin();
            TheRewriter.InsertText(ST, SSBefore.str(), true, true);
        }
        return true;
    }

    // document struct, union, enum module variable declarations
    bool VisitTagDecl(TagDecl *decl) {
        stringstream SSBefore;
        if (!decl->isEmbeddedInDeclarator()) {
            if (!hasDocComment(decl)) {
                
                if (decl->isStruct()) {
                    SSBefore << "/// @struct "<< decl->getNameAsString() << "\n";
                    SSBefore << "/// @brief TBD" << "\n";
                }
                if (decl->isUnion()) {
                    SSBefore << "/// @union "<< decl->getNameAsString() << "\n";
                    SSBefore << "/// @brief TBD" << "\n";
                }
                if (decl->isEnum() && !decl->isEmbeddedInDeclarator()) {
                    SSBefore << "/// @enum "<< decl->getNameAsString() << "\n";
                    SSBefore << "/// @brief TBD" << "\n";
                }
                SourceLocation ST = decl->getSourceRange().getBegin();
                TheRewriter.InsertText(ST, SSBefore.str(), true, true);
            }
        }
        //else{
        //    if (decl->isStruct()) {
        //        SSBefore << "/// @struct "<< decl->getNameAsString()<< "\n";
        //        SSBefore << "/// @brief TBD" << "\n";
        //    }
        //    SourceLocation ST = decl->getRBraceLoc();//decl->getLocation();//decl->getRBraceLoc();//getSourceRange().getEnd();
        //    TheRewriter.InsertText(ST, SSBefore.str(), true, true);
        //}
        return true;
    }
	
    // document class member variable declarations
    bool VisitFieldDecl(FieldDecl *decl) {
        //if (decl->isCXXClassMember()) {
        if (!hasDocComment(decl)) {
            stringstream SSBefore;
            SSBefore << "/// @var " << decl->getQualifiedNameAsString() << "\n";
            SSBefore << "/// @brief TBD" << "\n";
            SourceLocation ST = decl->getSourceRange().getBegin();
            TheRewriter.InsertText(ST, SSBefore.str(), true, true);
        }
        //}
        return true;
    }

    bool procFuncDecl(FunctionDecl *decl)
    {
        stringstream SSBefore;
        
        // Function name
        DeclarationName DeclName = decl->getNameInfo().getName();
        string FuncName = DeclName.getAsString();
        
        //        ASTContext& ctx = decl->getASTContext();
        //        SourceManager& sm = ctx.getSourceManager();
        //        SourceRange d_range = decl->getSourceRange();
        //        PresumedLoc dpl_start = sm.getPresumedLoc(d_range.getBegin());
        //        cerr<<"procFuncDecl ["<<FuncName<<" : "<<dpl_start.getFilename()<<"@line["<<dpl_start.getLine()<<"]:\n";
        //       if (!hasDocComment(decl)) {
        //            cerr<<"May already have comment ["<<FuncName<<"]\n";
        //        }
        
        if (!hasDocComment(decl)) {
            
            Stmt *FuncBody = decl->getBody();
            if (FuncBody) {
                
                // Add comment before
                // Type name as string
                QualType QT=decl->getReturnType();
                string TypeStr = QT.getAsString();
                
                unsigned NPARMS=decl->getNumParams();
                
                SSBefore << "/// @fn " << TypeStr <<" "<< decl->getQualifiedNameAsString() <<"(";
                if(NPARMS)
                    for (unsigned i=0;i<NPARMS;i++){
                        ParmVarDecl *PV = decl->getParamDecl(i);
                        QualType pvqt = PV->getOriginalType();
                        SSBefore<<pvqt.getAsString()<<" "<<PV->getNameAsString();
                        if (i<NPARMS-1) {
                            SSBefore<<", ";
                        }
                    }
                SSBefore << ")\n";
                SSBefore << "/// @brief TBD." << "\n";
                if(NPARMS)
                    for (unsigned i=0;i<NPARMS;i++){
                        ParmVarDecl *PV = decl->getParamDecl(i);
                        QualType pvqt = PV->getOriginalType();
                        SSBefore << "/// @param[in] "<<PV->getNameAsString()<<" TBD\n";
                    }
                SSBefore << "/// @return TBD\n";
                
                SourceLocation ST = decl->getSourceRange().getBegin();
                TheRewriter.InsertText(ST, SSBefore.str(), true, true);
                
                // And after
                stringstream SSAfter;
                SSAfter << "\n// End function " << FuncName << "\n";
                ST = FuncBody->getLocEnd().getLocWithOffset(1);
                TheRewriter.InsertText(ST, SSAfter.str(), true, true);
                
            }else{
                DeclarationName DeclName = decl->getNameInfo().getName();
                string FuncName = DeclName.getAsString();
                cerr<<"No body for ["<<FuncName<<"] ["<<(decl->hasSkippedBody()?"SKIPPED":"NOT SKIPPED")<<"]\n";
            }
        }
        else{
            ASTContext& ctx = decl->getASTContext();
            SourceManager& sm = ctx.getSourceManager();
            SourceRange d_range = decl->getSourceRange();
            PresumedLoc dpl_start = sm.getPresumedLoc(d_range.getBegin());
            PresumedLoc dpl_end = sm.getPresumedLoc(d_range.getEnd());
            cerr<<"procFuncDecl ["<<FuncName<<" Has comment: "<<\
            dpl_start.getFilename()<<"@line["<<\
            dpl_start.getLine()<<":"<<dpl_end.getLine()<<"]:\n";
        }
        
        return true;
    }
    
	// document (module and class member) function declarations
    bool VisitFunctionDecl(FunctionDecl *decl) {
        // Only function definitions (with bodies), not declarations.

        if (decl->isThisDeclarationADefinition()) {
            procFuncDecl(decl);
        }
//        else{
//            DeclarationName DeclName = decl->getNameInfo().getName();
//            string FuncName = DeclName.getAsString();
//            cerr<<"Not a definition ["<<FuncName<<"]\n";
//        }

        
        return true;
    }

    //    bool VisitStmt(Stmt *s) {
    //        // Only care about If statements.
    //        if (isa<IfStmt>(s)) {
    //            IfStmt *IfStatement = cast<IfStmt>(s);
    //            Stmt *Then = IfStatement->getThen();
    //
    //            TheRewriter.InsertText(Then->getLocStart(),
    //                                   " // the 'if' part\n",
    //                                   true, true);
    //
    //            Stmt *Else = IfStatement->getElse();
    //            if (Else)
    //                TheRewriter.InsertText(Else->getLocStart(),
    //                                       " // the 'else' part\n",
    //                                       true, true);
    //        }
    //
    //        return true;
    //    }
    
    //    bool VisitDeclaratorDecl(DeclaratorDecl *d) {
    //        stringstream SSBefore;
    //        SSBefore << "/// @var " << d->getQualifiedNameAsString() << "\n";
    //        SSBefore << "/// @brief TBD" << "\n";
    //        SourceLocation ST = d->getSourceRange().getBegin();
    //        TheRewriter.InsertText(ST, SSBefore.str(), true, true);
    //        return true;
    //    }

    //    bool VisitMacroDirective(MacroDirective *r) {
    //        stringstream SSBefore;
    //        MacroInfo *MI = decl->getMacroInfo();
    //        MI->dump();
    //        SSBefore << "/// @def " << MI->tokens_begin() << "\n";
    //        SSBefore << "/// @brief TBD" << "\n";
    //        SourceLocation ST = MI->getDefinitionLoc();
    //        TheRewriter.InsertText(ST, SSBefore.str(), true, true);
    //        return true;
    //    }

    //        bool VisitPreprocessingRecord(PreprocessingRecord *r) {
    //            stringstream SSBefore;
    //            clang::PreprocessingRecord::iterator it;
    //            for (it=decl->begin(); it != decl->end(); it++){
    //                PreprocessedEntity *pe = (PreprocessedEntity *)&(it);
    //                if (pe->getKind() == clang::PreprocessedEntity::MacroDefinitionKind ) {
    //                    SSBefore << ">>>>>>>>>>>>>Found a MacroDef!!!\n";
    //                    SourceLocation ST = pe->getSourceRange().getBegin();
    //                    TheRewriter.InsertText(ST, SSBefore.str(), true, true);
    //
    //                }else{
    //                    SSBefore << ">>>>>>>>>>>>>Found a  what???\n";
    //                    SourceLocation ST = pe->getSourceRange().getBegin();
    //                    TheRewriter.InsertText(ST, SSBefore.str(), true, true);
    //               
    //                }
    //            }
    //            return true;
    //        }

private:
    void AddBraces(Stmt *s);
	
    Rewriter &TheRewriter;

};


// Implementation of the ASTConsumer interface for reading an AST produced
// by the Clang parser.
class MyASTConsumer : public ASTConsumer
{
public:
    MyASTConsumer(Rewriter &R)
	: Visitor(R)
    {}
	
    // Override the method that gets called for each parsed top-level
    // declaration.
    virtual bool HandleTopLevelDecl(DeclGroupRef DR) {
            for (DeclGroupRef::iterator b = DR.begin(), e = DR.end();
                 b != e; ++b)
                // Traverse the declaration using our AST visitor.
                Visitor.TraverseDecl(*b);
        return true;
    }
	
private:
    MyASTVisitor Visitor;
};

// Preprocessor callback definitions
class MyPPCallback : public clang::PPCallbacks{
public:
    MyPPCallback(Rewriter &R, Preprocessor &P)
    : TheRewriter(R), ThePreprocessor(P)
    {}
    virtual ~MyPPCallback()
    {
        
    }
    
private:
    template<class C>
    bool hasDocComment(C* element){
        bool retval=false;
        stringstream SSBefore;
//       cerr<<"MyPPCallback::hasDocComment - Got here!!!!\n";

        SourceManager &sm = ThePreprocessor.getSourceManager();
        PresumedLoc element_ploc = sm.getPresumedLoc( element->getLocation());
        SourceLocation element_loc = element->getLocation();
        unsigned element_line = element_ploc.getLine();
        //           SSBefore<<"ELEMENT_LINE["<<element_line<<"]\n";
        
        int offset_to_macro_start = 0-sm.getSpellingColumnNumber(element_loc)+1;
        int offset_to_prev_tok_end = offset_to_macro_start-1;
        SourceLocation loc_of_prev_token_end = element_loc.getLocWithOffset(offset_to_prev_tok_end);
        Token prev_end_token;
        ThePreprocessor.getRawToken(loc_of_prev_token_end,prev_end_token,true);
        unsigned col_of_prev_tok_end = sm.getSpellingColumnNumber(loc_of_prev_token_end)-1;
        
        int offset_to_prev_tok_start=offset_to_prev_tok_end-(col_of_prev_tok_end-1);
        
        SourceLocation loc_of_prev_token_start=element_loc.getLocWithOffset(offset_to_prev_tok_start-1);
        
        //           SSBefore<<"ELEMENT_LIN_COL["<<sm.getSpellingLineNumber(element_loc)<<"," \
        //           <<sm.getSpellingColumnNumber(element_loc)<<"]\n";
        //           SSBefore<<"offset_to_prev_tok_end["<<offset_to_prev_tok_end<<"]\n";
        //           SSBefore<<"col_of_prev_tok_end["<<col_of_prev_tok_end<<"]\n";
        //           SSBefore<<"offset_to_prev_tok_start["<<offset_to_prev_tok_start<<"]\n";
        
        Token prev_start_token;
        if(!ThePreprocessor.getRawToken(loc_of_prev_token_start,prev_start_token,true)){

//            cerr<<"MyPPCallback::hasDocComment - Checking Comments!!!!\n";
            SSBefore<<"TOK["<<ThePreprocessor.getSpelling(prev_start_token)<<"]\n";
            SSBefore<<"TOK_LIN_COL["<<sm.getSpellingLineNumber(loc_of_prev_token_start)<<"," \
            <<sm.getSpellingColumnNumber(loc_of_prev_token_start)<<"]\n";
            
//            if (ThePreprocessor.getSpelling(prev_start_token).find("//")!=std::string::npos ||
//                ThePreprocessor.getSpelling(prev_start_token).find("/*")!=std::string::npos ||
//                ThePreprocessor.getSpelling(prev_start_token).find("*/")!=std::string::npos ||
//                ThePreprocessor.getSpelling(prev_start_token)=="*") {
//                SSBefore<<"!!! LOOKS LIKE WE HAVE OURSELVES A COMMENT\n";
//                retval=true;
//            }


                if (
                      ThePreprocessor.getSpelling(prev_start_token).find("/**")!=std::string::npos ||
                      ThePreprocessor.getSpelling(prev_start_token).find("//!")!=std::string::npos ||
                      ThePreprocessor.getSpelling(prev_start_token).find("/// @")!=std::string::npos ||
                      ThePreprocessor.getSpelling(prev_start_token).find("///@")!=std::string::npos) {
                                SSBefore<<"// !!! LOOKS LIKE WE HAVE OURSELVES A COMMENT\n";
//
//                    cerr<<"Preprocessor - Comment @ ["<< element_ploc.getFilename()<<" L"\
//                    <<sm.getSpellingLineNumber(loc_of_prev_token_start)<<" C" \
//                    <<sm.getSpellingColumnNumber(loc_of_prev_token_start)<<"]\n";
                retval=true;
            }
        }
        return retval;
    }
    
    // Preprocessor macro definition callback
   void 	MacroDefined (const Token &MacroNameTok, const MacroDirective *MD){
       if (MD->getKind()==MacroDirective::MD_Define) {
           
           
           stringstream SSBefore;
           
//           SourceManager &sm = ThePreprocessor.getSourceManager();
//           PresumedLoc md_start = sm.getPresumedLoc( MD->getLocation());
//           unsigned md_line = md_start.getLine();
////           SSBefore<<"MD_LINE["<<md_line<<"]\n";
//           
//           int offset_to_macro_start = 0-sm.getSpellingColumnNumber(MD->getLocation())+1;
//           int offset_to_prev_tok_end = offset_to_macro_start-1;
//           SourceLocation loc_of_prev_token_end = MD->getLocation().getLocWithOffset(offset_to_prev_tok_end);
//           Token prev_end_token;
//           ThePreprocessor.getRawToken(loc_of_prev_token_end,prev_end_token,true);
//           unsigned col_of_prev_tok_end = sm.getSpellingColumnNumber(loc_of_prev_token_end)-1;
//           
//           int offset_to_prev_tok_start=offset_to_prev_tok_end-(col_of_prev_tok_end-1);
//           
//           SourceLocation loc_of_prev_token_start=MD->getLocation().getLocWithOffset(offset_to_prev_tok_start-1);
//           
////           SSBefore<<"MD_LIN_COL["<<sm.getSpellingLineNumber(MD->getLocation())<<"," \
////           <<sm.getSpellingColumnNumber(MD->getLocation())<<"]\n";
////           SSBefore<<"offset_to_prev_tok_end["<<offset_to_prev_tok_end<<"]\n";
////           SSBefore<<"col_of_prev_tok_end["<<col_of_prev_tok_end<<"]\n";
////           SSBefore<<"offset_to_prev_tok_start["<<offset_to_prev_tok_start<<"]\n";
//           
//           Token prev_start_token;
//           if(!ThePreprocessor.getRawToken(loc_of_prev_token_start,prev_start_token,true)){
//               SSBefore<<"TOK["<<ThePreprocessor.getSpelling(prev_start_token)<<"]\n";
//               SSBefore<<"TOK_LIN_COL["<<sm.getSpellingLineNumber(loc_of_prev_token_start)<<"," \
//               <<sm.getSpellingColumnNumber(loc_of_prev_token_start)<<"]\n";
//
//               if (ThePreprocessor.getSpelling(prev_start_token).find("//")!=std::string::npos ||
//                   ThePreprocessor.getSpelling(prev_start_token).find("/*")!=std::string::npos ||
//                   ThePreprocessor.getSpelling(prev_start_token).find("*/")!=std::string::npos ||
//                   ThePreprocessor.getSpelling(prev_start_token)=="*") {
////                   SSBefore<<"!!! LOOKS LIKE WE HAVE OURSELVES A COMMENT\n";
//               }else{
//               
//               }
//           }
           
           if (!hasDocComment(MD)) {
               SSBefore << "/// @def "<<ThePreprocessor.getSpelling(MacroNameTok);
               
               const MacroInfo *MI = MD->getMacroInfo();
               if (!MI->arg_empty()) {
                   
                   SSBefore<<"(";
                   
                   ArrayRef<const IdentifierInfo *> args = MI->args();
                   unsigned i=0;
                   for (i=0; i<MI->getNumArgs(); i++) {
                       SSBefore << args[i]->getNameStart();
                       if (i<(MI->getNumArgs()-1)) {
                           SSBefore<<",";
                       }
                   }
                   SSBefore <<")";
               }
               SSBefore << "\n";
               SSBefore<<"/// @brief TBD\n";
               
               int offset=-1;
               SourceLocation st = MD->getLocation().getLocWithOffset(offset--);
               Token t;
               ThePreprocessor.getRawToken(st,t,true);
               
               while( ThePreprocessor.getSpelling(t)[0]!='#'){
                   //               SSBefore<<"TOKEN:os["<<offset<<"]:["<<ThePreprocessor.getSpelling(t)<<"]\n";
                   st = MD->getLocation().getLocWithOffset(offset--);
                   ThePreprocessor.getRawToken(st,t,true);
               }
               
               SourceLocation ST = MD->getLocation().getLocWithOffset(offset+1);
               TheRewriter.InsertText(ST, SSBefore.str(), true, true);

           }
       }
     }

    Rewriter &TheRewriter;
    Preprocessor &ThePreprocessor;
};

int main(int argc, char *argv[])
{
    int retval=0;
    char *includes[32]={0};
    int incl_count=0;
    
    if (argc < 2) {
        llvm::errs() << "\n Usage: "<<basename(argv[0])<<" [-I include_path...] <filename>\n\n";
        retval = 1;
    }else{
        if (argc>2) {
            for(int i=0;i<argc;i++){
                if (strstr(argv[i],"-I")) {
                    includes[incl_count++]=argv[i+1];
                    i++;
                }
            }
        }
    // CompilerInstance will hold the instance of the Clang compiler for us,
    // managing the various objects needed to run the compiler.
    CompilerInstance TheCompInst;
    // TheCompInst.createDiagnostics(0,0);
    DiagnosticConsumer *DC=NULL;
    TheCompInst.createDiagnostics(DC, true);
    
    // init include search path
    clang::HeaderSearchOptions & headerSearchOptions = TheCompInst.getHeaderSearchOpts();
    
    // IncludeDirGroup::Angled,After, Quoted
    //	headerSearchOptions.AddPath("/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.10.sdk/usr/include", clang::frontend::IncludeDirGroup::After, false, false);
    //    headerSearchOptions.AddPath("/Library/Developer/CommandLineTools/usr/include", clang::frontend::IncludeDirGroup::Angled, false, false);
    //    headerSearchOptions.AddPath("/Library/Developer/CommandLineTools/usr/include/c++/v1", clang::frontend::IncludeDirGroup::Angled, false, false);
    //    headerSearchOptions.AddPath("/Users/headley/Downloads/apps/llvm-3.6.2.src/tools/clang/include", clang::frontend::IncludeDirGroup::Angled, false, false);
    //    headerSearchOptions.AddPath("/usr/include/c++/4.2.1", clang::frontend::IncludeDirGroup::Angled, false, false);
    //	  headerSearchOptions.AddPath("/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.9.sdk/usr/include", clang::frontend::IncludeDirGroup::Angled, false, false);
    
    // set this to true to see include and other search paths
    headerSearchOptions.Verbose = false;
    //	headerSearchOptions.UseStandardCXXIncludes = 0;
    //	headerSearchOptions.UseStandardSystemIncludes = 0;
    //	headerSearchOptions.UseBuiltinIncludes = 1;
    //	headerSearchOptions.UseLibcxx = 1;
    //	headerSearchOptions.ResourceDir = "/Users/headley/Downloads/apps/llvm-3.6.2.src/tools/clang/include";
        for(int i=0;i<incl_count;i++){
            headerSearchOptions.AddPath(includes[i],clang::frontend::IncludeDirGroup::Angled,false,false);
            //fprintf(stderr,"adding include path [%d/%d] [%s]\n",i,incl_count,includes[i]);
        }
//        headerSearchOptions.AddPath("./oasis5/src/controller/include",clang::frontend::IncludeDirGroup::Angled,false,false);
//        headerSearchOptions.AddPath("./oasis5/src/drivers",clang::frontend::IncludeDirGroup::Angled,false,false);
//        headerSearchOptions.AddPath("./oasis5/src/FreeRTOS/include",clang::frontend::IncludeDirGroup::Angled,false,false);
//        headerSearchOptions.AddPath("./oasis5/src/fatFs/include",clang::frontend::IncludeDirGroup::Angled,false,false);
        
    // configure language options
    clang::LangOptions& langOptions = TheCompInst.getLangOpts();
    langOptions.CPlusPlus = 1;
    langOptions.Bool = 1;
    langOptions.RTTI = 0;
    //	langOptions.GNUMode = 1;
    //	langOptions.Exceptions = 0;
    
    // Initialize target info with the default triple for our platform.
    //    TargetOptions TO;
    //    TO.Triple = llvm::sys::getDefaultTargetTriple();
    
    const std::shared_ptr<clang::TargetOptions> TO = std::make_shared<clang::TargetOptions>();
    TO->Triple = llvm::sys::getDefaultTargetTriple();
    
    TargetInfo *TI = TargetInfo::CreateTargetInfo(
                                                  TheCompInst.getDiagnostics(), TO);
    TheCompInst.setTarget(TI);
    
    TheCompInst.createFileManager();
    FileManager &FileMgr = TheCompInst.getFileManager();
    TheCompInst.createSourceManager(FileMgr);
    SourceManager &SourceMgr = TheCompInst.getSourceManager();
    TheCompInst.createPreprocessor(TU_Complete);
    TheCompInst.createASTContext();
    
    // A Rewriter helps us manage the code rewriting task.
    Rewriter TheRewriter;
    TheRewriter.setSourceMgr(SourceMgr, TheCompInst.getLangOpts());
    
    // Set the main file handled by the source manager to the input file.
    const FileEntry *FileIn = FileMgr.getFile(argv[argc-1]);
    // SourceMgr.createMainFileID(FileIn);
    SourceMgr.setMainFileID( SourceMgr.createFileID( FileIn, clang::SourceLocation(), clang::SrcMgr::C_User));
    TheCompInst.getDiagnosticClient().BeginSourceFile(
                                                      TheCompInst.getLangOpts(),
                                                      &TheCompInst.getPreprocessor());
    
    // turn off diagnostics (errors/warnings), since we're translating
    // rather than compiling
    TheCompInst.getDiagnostics().setSuppressAllDiagnostics(true);
    
    // register preprocessor callbacks
    MyPPCallback *ppc = new MyPPCallback(TheRewriter, TheCompInst.getPreprocessor());
    TheCompInst.getPreprocessor().addPPCallbacks( (std::unique_ptr<PPCallbacks>)(ppc));
    
    // Create an AST consumer instance which is going to get called by
    // ParseAST.
    MyASTConsumer TheConsumer(TheRewriter);
    
        CommentOptions CommentOpts;
        CommentOpts.ParseAllComments=true;
    TheCompInst.getASTContext().getCommentCommandTraits().registerCommentOptions(CommentOpts);
    
    // Parse the file to AST, registering our consumer as the AST consumer.
    ParseAST(TheCompInst.getPreprocessor(), &TheConsumer,
             TheCompInst.getASTContext());
    
    
    // At this point the rewriter's buffer should be full with the rewritten
    // file contents.
    const RewriteBuffer *RewriteBuf =
    TheRewriter.getRewriteBufferFor(SourceMgr.getMainFileID());
        if (RewriteBuf) {
            llvm::outs() << string(RewriteBuf->begin(), RewriteBuf->end());
        }
    }
    return retval;
}

