#!/bin/bash

#########################################
# Name: doxify.sh
#
# Summary: Populate source code with
#          Doxygen comment stubs
#
# Description:
#
# Author: k headley
#
# Copyright MBARI 2016
#
#########################################

#########################################
# Script configuration defaults
# casual users should not need to change
# anything below this section
#########################################
description="Populate source code with Doxygen comment stubs"

#################################
# Script variable initialization
#################################
DX_VERBOSE_DFL=${DX_VERBOSE_DFL:-"N"}
DX_DEBUG_DFL=${DX_DEBUG_DFL:-"N"}
DX_TEST_DFL=${DX_TEST_DFL:-"N"}
DX_KEEP_DFL=${DX_KEEP_DFL:-"N"}
DX_RECURSIVE_DFL=${DX_RECURSIVE_DFL:-"N"}
DX_OPATH_DFL=${DX_OPATH_DFL:-"./dxout"}
DX_HOME=${DX_HOME:-"./doxifier-ast"}
DX_DOXIFY_DFL=${DX_DOXIFY_DFL:-"doxify"}
DX_DOXIFY_PP_DFL=${DX_DOXIFY_PP_DFL:-"doxifier-pp.sh"}
DX_EXT_DFL=${DX_EXT_DFL:-"dx"}

AWK=gawk

declare -a RAW_FILES
#declare -a WFILES
#declare -a WTOPS

unset DX_OPATH
unset DX_VERBOSE
unset DX_DEBUG
unset DX_TEST
unset DX_KEEP
unset DX_RECURSIVE
unset DX_ARGS
unset DX_WDIR
unset DX_WFILE
unset DX_CONF

#################################
# Function Definitions
#################################
#################################
# name: sigTrap
# description: signal trap callback
# will interrupt 
# args: none
#################################
sigTrap(){
    exit 0;
}

#################################
# name: printUsage
# description: print use message
# args: none
#################################
printUsage(){
    echo
    echo "Description: $description"
    echo
    echo "usage: `basename $0` [options] [<file/dir>[,match]...]"
	echo ""
    echo "Where match is a substring of the file/dir path to"
	echo "duplicate in the output directory"
	echo ""
	echo "Options:"
	echo "-f <file> : config file"
	echo "-k        : keep temp files   [$DX_KEEP_DFL]"
	echo "-o <dir>  : output path       [$DX_OPATH_DFL]"
	echo "-p <scr>  : preprocess cmd    [$DX_DOXIFY_PP]"
	echo "-r        : recursive         [$DX_RECURSIVE_DFL]"
	echo "-t        : test              [$DX_TEST_DFL]"
	echo "-D        : debug output      [$DX_DEBUG_DFL]"
	echo "-V        : verbose output    [$DX_VERBOSE_DFL]"
    echo "-h        : print use message"
    echo ""
    echo
}

########################################
# name: vout
# description: print DX_VERBOSE message to stderr
# args:
#     msg: message
########################################
vout(){
    if [ "${DX_VERBOSE}" == "Y" ] || [ "${DX_VERBOSE}" == "TRUE" ]
    then
	echo "$1" >&2
    fi
}
dout(){
    if [ "${DX_DEBUG}" == "Y" ] || [ "${DX_DEBUG}" == "TRUE" ]
    then
    echo "$1" >&2
    fi
}

########################################
# name: imsg
# description: echos with indent
# args:
#     msg: message
#  indent: indent level
########################################
imsg(){
  OIFS=$IFS
  IFS=""
  msg_pad="                                        "
  let "msg_indent=${1}"
  shift
  all="${@}"
  printf "%s%s\n" ${msg_pad:0:${msg_indent}} ${all}
  IFS=$OIFS
}

########################################
# name: msg
# description: echos with default indent
# args:
#     msg: message
########################################
msg(){
 imsg $MSG_INDENT "${*}"
}

########################################
# name: exitError
# description: print use message to stderr
# args:
#     msg:        error message
#     returnCode: exit status to return
########################################
exitError(){
    echo >&2
    echo "`basename $0`: error - $1" >&2
    echo >&2
    exit $2
}

########################################
# name: processCmdLine
# description: do command line processsing
# args:
#     args:       positional paramters
#     returnCode: none
########################################
processCmdLine(){
        OPTIND=1
        vout "`basename $0` all args[$*]"
		while getopts f:hko:p:rtDV Option
        do
        vout "`basename $0` Processing option - $Option[$OPTARG]"
        case $Option in
			f ) DX_CONF=${OPTARG}
            ;;
            k ) export DX_KEEP="Y"
            ;;
            o ) export DX_OPATH=${OPTARG}
            ;;
			p ) export DX_DOXIFY_PP=${OPTARG}
			;;
            r ) export DX_RECURSIVE="Y"
            ;;
            t ) export DX_TEST="Y"
            ;;
            D ) export DX_DEBUG="Y"
            ;;
			V ) export DX_VERBOSE="Y"
			;;
			h) printUsage
				exit 0
			;;
            *) let "argopt=$OPTIND-1"
            exitError "unrecognized option [${@:$argopt:1} ${@:$OPTIND:1}]"
            ;;
		esac
	done
}

########################################
# name: dx_validate_opts
# description: validate options
# args:
#     returnCode: none
########################################
dx_validate_opts(){
vout "validating options..."

DX_TEST="${DX_TEST:-${DX_TEST_DFL}}"

if [ ! -d "${DX_OPATH}" ]
then
	vout "output directory [${DX_OPATH}] does not exist"
	if [ "${DX_TEST}" != "Y" ]
	then
		#echo mkdir -p ${DX_OPATH}
		echo "will create output directory [${DX_OPATH}]"
	fi
fi
foo="${DX_HOME}/${DX_DOXIFY_DFL}"
DX_DOXIFY="${DX_DOXIFY:-$foo}"
#foo="${DX_HOME}/${DX_DOXIFY_PP_DFL}"
#DX_DOXIFY_PP=${DX_DOXIFY_PP:-$foo}

if [ ! -f "${DX_DOXIFY}" ]
then
	echo "DOXIFY not found [${DX_DOXIFY}]"
fi

if [ ! -z "${DX_DOXIFY_PP}" ] && [ ! -f "${DX_DOXIFY_PP}" ]
then
	echo "DOXIFY_PP not found [${DX_DOXIFY_PP}]"
fi

DX_EXT=${DX_EXT:-$DX_EXT_DFL}
DX_KEEP=${DX_KEEP:-$DX_KEEP_DFL}

}

########################################
# name: dx_mkdir
# description: create directory if it
#              doesn't exist
# args:
#     $1 source
#     $2 dest
#     $3 match_from
#     returnCode: none
########################################
dx_mkdir(){

local src=${1}
local des=${2}
local match_from=${3}
local retval=""
local src_dir=""
local tmp_dir=""
local match_idx=""
local out_dir=""

dout "src[$src]"
dout "des[$des]"
dout "match_from[$match_from]"

if [ -f "${src}" ]
then
tmp_dir=`dirname ${src}`
# convert relative path to absolute, if needed
src_dir=$(cd $tmp_dir;pwd)
elif [ -d "${src}" ]
then
# convert relative path to absolute, if needed
src_dir=$(cd $src;pwd)
else
vout "dx_mkdir - src not found [${src}]"
fi

    if [ "${#match_from}" -gt 0 ]
    then
        match_idx=`expr "${src_dir}" : .*${match_from}`
        if [ "${match_idx}" -gt 0 ]
        then
        tmp_dir=${match_from}${src_dir#*${match_from}*}
        else
        tmp_dir=${src_dir}
        fi
    else
        tmp_dir=""
    fi

    tmp_dir=${tmp_dir#/*}
    out_dir="${des}/${tmp_dir}"



if [ "${tmp_dir}" == "" ]
then
out_dir="${des}"
fi

dout "src[$src]"
dout "des[$des]"
dout "match_from[$match_from]"
dout "match_idx[$match_idx]"
dout "out_dir[$out_dir]"

if [ ! -d "${out_dir}" ]
then
    if [ "${DX_TEST}" == "N" ]
    then
	vout "creating out_dir : ${out_dir}"
    mkdir -p ${out_dir}
    else
	DX_VOLD=$DX_VERBOSE
	DX_VERBOSE="Y"
	vout "TEST : mkdir -p ${out_dir}"
	DX_VERBOSE=${DX_VOLD}
    fi
else
dout "${out_dir} exists"
fi
echo "${out_dir}"

}

########################################
# name: dx_process_file
# description: process a file
# args:
#     $0 destination
#      $n files
#     returnCode: none
########################################
dx_process_file(){
	local ipath=${1}
	local opath=${2}
	local top=${3}
    local idir=`dirname ${ipath}`
	local ifile=`basename ${ipath}`
idir=$(cd $idir;pwd)
dout "ipath[$ipath]"
dout "opath[$opath]"
dout "top[$top]"
dout "idir[$idir]"

	if [ "${idir}" == "." ]
	then
		idir=`basename $opath`
	fi

	dest=$(dx_mkdir ${idir} ${opath} ${top})

	tmp_file="${dest}/${ifile}.${DX_EXT}"
	dox_out="${dest}/${ifile}"

#dout "dest[$dest] ifile[ifile] idir[$idir] opath[$opath] top[$top]"

    if [ "${DX_TEST}" == "N" ]
    then
# pre-process (creates temporary working copy
		#$AWK -f ${DX_DOXIFY_PP}
		if [ ! -z ${DX_DOXIFY_PP} ]
		then
			vout "Preprocessing using ${DX_DOXIFY_PP}"
			${DX_DOXIFY_PP} ${ipath} > ${tmp_file}
		else
			vout "[no preprocess]"
			cat ${ipath} > ${tmp_file}
        fi

# parse and emit Doxygen stubs
		${DX_DOXIFY} ${DX_ARGS} ${tmp_file} > ${dox_out}
# remove temporary file
        if [ "${DX_KEEP}" == "N" ]
        then
			rm ${tmp_file}
        fi
    else
        echo "$AWK -f ${DX_DOXIFY_PP}  ${ipath} > ${tmp_file}"
        echo "${DX_DOXIFY} ${tmp_file} > ${dox_out}"
        if [ "${DX_KEEP}" == "N" ]
        then
            echo "rm ${tmp_file}"
		else
			echo "keep ${tmp_file}"
        fi
    fi

}

########################################
# name: dx_doxify
# description: process files
# args:
#     returnCode: none
########################################
dx_doxify(){
    vout "processing files..."
	vout "RAW_FILES : ${RAW_FILES[@]}"
	for FILE in "${RAW_FILES[@]}"
    do
        unset SRC_PATH
		unset PATH_MATCH

		# split source path and match (if specified)
        IFS="," read -r SRC_PATH PATH_MATCH <<< "$FILE"

		# if no match provided, use full source path
		if	[ -z "${PATH_MATCH}" ]
		then
			if [ -f "${FILE}" ]
			then
				xdir=`dirname ${SRC_PATH}`
				PATH_MATCH=`echo ${xdir}|sed -e "s/^[./]*//g"`
			elif [ -d "${FILE}" ]
			then
				PATH_MATCH=`echo ${SRC_PATH}|sed -e "s/^[./]*//g"`
			fi
		fi
vout "FILE[$FILE] SRC_PATH[${SRC_PATH}] PATH_MATCH[${PATH_MATCH}]"

    	# process files, directories
        if [ -f "${SRC_PATH}" ]
        then
			vout ""
			vout "processing file [${SRC_PATH}]"
			dx_process_file ${SRC_PATH} ${DX_OPATH} ${PATH_MATCH}
        elif [ -d "${SRC_PATH}" ]
        then
			vout ""
			vout "processing dir[${SRC_PATH}] top[${PATH_MATCH}]"
				for FFILE in `find  -E ${SRC_PATH} -regex "(.*\.c)|(.*\.cpp)|(.*\.h)"`
				do
            vout ""
            vout "processing file [${FFILE}]"
					filedir=`dirname $FFILE`

					if [ "${SRC_PATH}" == "${filedir}" ]
					then
						dx_process_file ${FFILE} ${DX_OPATH} ${PATH_MATCH}
					else
                        if [ "${DX_RECURSIVE}" == "Y"  ]
                        then
	                        dx_process_file ${FFILE} ${DX_OPATH} ${PATH_MATCH}
                        else
    	                    vout "recursion disabled"
                        fi
					fi
                done
        else
            echo "file/directory not found : [${SRC_PATH}]"
        fi

    done
}

########################################
# name: dx_test
# description: test functions
# args:
#     returnCode: none
########################################
dx_test(){
vout
dx_mkdir . /Users/headley/tmp
vout
dx_mkdir ./ /Users/headley/tmp
vout
dx_mkdir . /Users/headley/tmp foo
vout
dx_mkdir ./ /Users/headley/tmp foo
vout
dx_mkdir ~/cvs/tmp/oasis5/test/fileTest /Users/headley/tmp oasis5
vout
dx_mkdir ~/cvs/tmp/oasis5/test/fileTest/buffer.c /Users/headley/tmp oasis5
vout
dx_mkdir xlat/hello.cpp /Users/headley/tmp xlat
vout
vout "dx_test - done"
}

##########################
# Script main entry point
##########################

# call sigTrap on INT,TERM or EXIT
# trap sigTrap INT TERM EXIT

# reset trapped signals
# trap - INT TERM EXIT

# process args (read cmd line)
# to get config file, if specified
processCmdLine $*

# apply config file settings (source)
if [ "${DX_CONF}" ] && [ -f "${DX_CONF}" ]
then
	vout "sourcing config [$DX_CONF]..."
	source ${DX_CONF}
fi

# apply cmd line settings
# (override config file)
processCmdLine $*


# check options
dx_validate_opts

# remove command line options
# leaving file/dir list
let "i=$OPTIND-1"
while [ "${i}" -gt 0  ]
do
shift
let "i-=1"
done

read -a RAW_FILES <<< $*

vout "DX_OPATH     : ${DX_OPATH}"
vout "DX_CONF      : ${DX_CONF}"
vout "DX_HOME      : ${DX_HOME}"
vout "DX_DOXIFY    : ${DX_DOXIFY}"
vout "DX_DOXIFY_PP : ${DX_DOXIFY_PP}"
vout "DX_ARGS      : ${DX_ARGS}"
vout "DX_VERBOSE   : ${DX_VERBOSE}"
vout "DX_DEBUG     : ${DX_DEBUG}"
vout "DX_TEST      : ${DX_TEST}"
vout "DX_KEEP      : ${DX_KEEP}"
vout "DX_RECURSIVE : ${DX_RECURSIVE}"
vout "SOURCES      : ${RAW_FILES[@]}"

# process files
dx_doxify
#dx_test

exit 0

