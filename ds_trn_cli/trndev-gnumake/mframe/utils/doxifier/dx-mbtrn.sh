#!/bin/bash


SRC_PATH="src"
DEST_PATH="dxout"

./doxifier-ast/doxify.sh -r -k -VD -f dx-mbtrn.conf -o ${DEST_PATH}  ${SRC_PATH}

#./doxifier-ast/doxify.sh -r -f dx-oasis5.conf -o ${DEST_PATH}/drivers  ${SRC_PATH}/drivers
#./doxifier-ast/doxify.sh -r -f dx-oasis5.conf -o ${DEST_PATH}/include  ${SRC_PATH}/include
#./doxifier-ast/doxify.sh -r -f dx-oasis5.conf -o ${DEST_PATH}/fatFs    ${SRC_PATH}/fatFs
#./doxifier-ast/doxify.sh -r -f dx-oasis5.conf -o ${DEST_PATH}/FreeRTOS ${SRC_PATH}/FreeRTOS
#./doxifier-ast/doxify.sh -r -f dx-oasis5.conf -o ${DEST_PATH}/PIC32    ${SRC_PATH}/PIC32
#./doxifier-ast/doxify.sh -r -f dx-oasis5.conf -o ${DEST_PATH}/system   ${SRC_PATH}/system
