///
/// @file xf-xhash.h
/// @authors k. headley
/// @date 06 nov 2012
/// 
/// xFOCE xhash API. xhash holds key:xvalue pairs to represent and serialize state.
/// 
/// @sa doxygen-examples.c for more examples of Doxygen markup

/////////////////////////
// Terms of use 
/////////////////////////
/*
 Copyright Information
 
 xFOCE - software for ocean acidification experiments
 Copyright 2002-2013 MBARI
 Monterey Bay Aquarium Research Institute, all rights reserved.
 
 Terms of Use
 
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version. You can access the GPLv3 license at
 http://www.gnu.org/licenses/gpl-3.0.html
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details 
 (http://www.gnu.org/licenses/gpl-3.0.html)
 
 MBARI provides the documentation and software code "as is", with no warranty,
 express or implied, as to the software, title, non-infringement of third party 
 rights, merchantability, or fitness for any particular purpose, the accuracy of
 the code, or the performance or results which you may obtain from its use. You 
 assume the entire risk associated with use of the code, and you agree to be 
 responsible for the entire cost of repair or servicing of the program with 
 which you are using the code.
 
 In no event shall MBARI be liable for any damages, whether general, special,
 incidental or consequential damages, arising out of your use of the software, 
 including, but not limited to, the loss or corruption of your data or damages 
 of any kind resulting from use of the software, any prohibited use, or your 
 inability to use the software. You agree to defend, indemnify and hold harmless
 MBARI and its officers, directors, and employees against any claim, loss, 
 liability or expense, including attorneys' fees, resulting from loss of or 
 damage to property or the injury to or death of any person arising out of the 
 use of the software.
 
 The MBARI software is provided without obligation on the part of the 
 Monterey Bay Aquarium Research Institute to assist in its use, correction, 
 modification, or enhancement.
 
 MBARI assumes no responsibility or liability for any third party and/or 
 commercial software required for the database or applications. Licensee agrees 
 to obtain and maintain valid licenses for any additional third party software 
 required.
 */

// Always do this
#ifndef XF_XHASH_H
#define XF_XHASH_H

#ifdef __cplusplus
extern "C" {
#endif
	
	/////////////////////////
	// Includes 
	/////////////////////////
//#include "xf-xvalue.h"
// for gwcfg_t
//#include "xf-types.h"
#include "mframe.h"
#include "mlist.h"

	/////////////////////////
	// Type Definitions
	/////////////////////////
    
    typedef struct mhash_s mhash_t;
    typedef struct mhash_t zhash_t;
    typedef struct mlist_t zlist_t;

	typedef bool (* xhash_compare_fn)(void *a, void *b);
	typedef void (* xhash_dump_fn)(void *item, int indent);
	typedef size_t (* xhash_serialw_fn)(void *self, byte **dest);
	typedef void * (* xhash_serialr_fn)(byte *data, byte **next);
	
	/////////////////////////
	// Declarations
	/////////////////////////
		
	/////////////////////////
	// Macros
	/////////////////////////
	
	/////////////////////////
	// Exports
	/////////////////////////
	
	void
	xhash_free(void *self);

	bool
	xhash_str_key_compare(void *a, void *b);
	
	void
	xhash_set_freefn(zhash_t *self, zhash_free_fn free_fn);

//    int
//    xhash_init(zhash_t *self, gwcfg_t *gwc);
	
	int
	xhash_insert_f(zhash_t *self, char *key, void *item, bool free_key, zhash_free_fn freefn);

	int
	xhash_insert_v(zhash_t *self, char *key, xvalue_t *item);

	int
	xhash_insert_cp(zhash_t *self, const char *key, void *item);
	
	bool
	xhash_has_key(zhash_t *self, const char *key);

	zlist_t *
	xhash_lkey_set(zlist_t *keys, const char *path, xhash_compare_fn compfn);

	zlist_t *
	xhash_key_set(zhash_t *self, const char *path);
	
	size_t
	xhash_string_bwrite(void *self, byte **dest);
	
	char *
	xhash_string_bread(byte *raw, byte **ptr);

	void *
	xhash_string_vbread(byte *raw, byte **ptr);

	zhash_t *
	xhash_xval_bytes2hash(byte *src, size_t size);
	
	zhash_t *
	xhash_str_bytes2hash(byte *src, size_t size);

	size_t
	xhash_xval_hash2bytes(zhash_t *self, byte **dest);
	
	size_t
	xhash_str_hash2bytes(zhash_t *self, byte **dest);
		
	
	size_t
	xhash_vkxv_hash2bytes(byte **buf, ...);
	
	void 
	xhash_str_dumpfn(void *item, int indent);

	void 
	xhash_idump(zhash_t *self, int indent, bool dump_items, xhash_dump_fn item_dump);
	
	void 
	xhash_dump(zhash_t *self);
	
	int
	xhash_test(bool verbose);
		

#ifdef __cplusplus
}
#endif

#endif // XF_XHASH_H 
