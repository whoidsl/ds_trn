///
/// @file xf-xhash.c
/// @authors k. Headley
/// @date 06 nov 2012
/// 
/// xFOCE xhash implementation, provides xFOCE utilities for zmq zhash_t.

/////////////////////////
// Terms of use 
/////////////////////////
/*
 Copyright Information
 
 xFOCE - software for ocean acidification experiments
 Copyright 2002-2013 MBARI
 Monterey Bay Aquarium Research Institute, all rights reserved.
 
 Terms of Use
 
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version. You can access the GPLv3 license at
 http://www.gnu.org/licenses/gpl-3.0.html
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details 
 (http://www.gnu.org/licenses/gpl-3.0.html)
 
 MBARI provides the documentation and software code "as is", with no warranty,
 express or implied, as to the software, title, non-infringement of third party 
 rights, merchantability, or fitness for any particular purpose, the accuracy of
 the code, or the performance or results which you may obtain from its use. You 
 assume the entire risk associated with use of the code, and you agree to be 
 responsible for the entire cost of repair or servicing of the program with 
 which you are using the code.
 
 In no event shall MBARI be liable for any damages, whether general, special,
 incidental or consequential damages, arising out of your use of the software, 
 including, but not limited to, the loss or corruption of your data or damages 
 of any kind resulting from use of the software, any prohibited use, or your 
 inability to use the software. You agree to defend, indemnify and hold harmless
 MBARI and its officers, directors, and employees against any claim, loss, 
 liability or expense, including attorneys' fees, resulting from loss of or 
 damage to property or the injury to or death of any person arising out of the 
 use of the software.
 
 The MBARI software is provided without obligation on the part of the 
 Monterey Bay Aquarium Research Institute to assist in its use, correction, 
 modification, or enhancement.
 
 MBARI assumes no responsibility or liability for any third party and/or 
 commercial software required for the database or applications. Licensee agrees 
 to obtain and maintain valid licenses for any additional third party software 
 required.
 */

/////////////////////////
// Headers 
/////////////////////////

//#include <string.h>
//#include <czmq.h>

#include "mhash.h"
#include "mmem.h"

//#include "xf-xhash.h"
//#include "xf-xhash-init.h"
//#include "xf-xvalue.h"
//#include "xf-memory.h"
//#include "xf-log.h"
//#include "xf-types.h"
//#include "xf-config.h"

/////////////////////////
// Macros
/////////////////////////

/////////////////////////
// Declarations 
/////////////////////////
//extern gwcfg_t *
//gwcfg_new(xfgw_t *gateway);
//
//extern env_config_t *
//gwcfg_env(gwcfg_t *self);
//
//extern slog_t *
//env_syslog_config(env_config_t *self);
//
//extern gwcfg_t *
//load_config(gwcfg_t *self, const char *alt_path);
//
//static struct gateway_config_s *
//s_xhash_stub_config(char *cfg_file);

struct mhash_s{
    
};

static bool
s_key_compare(void *a, void *b);

//static off_t
//xhash_bwrite_keys(zhash_t *self, zlist_t *key_set, byte **dest);

static size_t
xhash_bwrite(zhash_t *self, zlist_t *key_list, byte **dest, xhash_serialw_fn serial_fn);


/////////////////////////
// Imports
/////////////////////////

/////////////////////////
// Module Global Variables
/////////////////////////

/// path lookup flags
typedef enum{
	ANY     = 0x01,
	ISTARTP = 0x02,
	PSTARTI = 0x04,
	IHASP   = 0x08,
	PHASI   = 0x10,
	MATCHES = 0x20,
	ICASE   = 0x40
}xvalue_mflags_t;


/////////////////////////
// Function Definitions
/////////////////////////

/// build xvalue hash table
static void 
s_init_item_hash(zhash_t *self, gwcfg_t *gwc)
{
	if (gwc == NULL) {
		
		// if no config passed in,
		// use stub
		gwc = s_xhash_stub_config(GW_CONFIG_DFL_FILE);
		
		xhash_hinit_platform(self,gwc);
		xhash_hinit_service(self,gwc);
		zhash_autofree(self);
		gwcfg_destroy(&gwc);
		
		return;
	}
	
	xhash_hinit_platform(self,gwc);
	xhash_hinit_service(self,gwc);
	
	
	// build other parts of the hash...
	
	return;
}

static bool
s_key_compare(void *a, void *b)
{
	
	if (a && b) {
		char *key_a = (char *)a;
		char *key_b = (char *)b;
		
		if (strncasecmp(key_a,key_b,strlen(key_a))>=0 && strstr(key_a,key_b)==key_a) {
			return true;
		}
	}
	return false;
}

/// true if a contains at least all of b 
//static bool
//s_key_match(void *a, void *b)
//{
//	if (a && b) {
//		char *key_a = (char *)a;
//		char *key_b = (char *)b;
//		
//		if (key_a && key_b && (strstr(key_a, key_b) != NULL) ) {
//			return true;
//		}
//	}
//	return false;
//}

static struct gateway_config_s *
s_xhash_stub_config(char *cfg_file)
{
	struct gateway_config_s *gwc = gwcfg_new(NULL);//NULL;//
	
	char *pconfig_path=NULL;

	struct environment_config_s *env=gwcfg_env(gwc);
	
	env_init(env,NULL, cfg_file, NULL, NULL);
	
	
	slog_t *syslog=env_syslog_config(env);
	slog_init(syslog);
	
	if(load_config(gwc,pconfig_path)==NULL){
		return NULL;
	}
	
	return gwc;
}

void xhash_free(void *self)
{
	zhash_destroy((zhash_t **)&self);
}

bool
xhash_str_key_compare(void *a, void *b)
{
	return s_key_compare(a,b);
}

void
xhash_set_freefn(zhash_t *self, zhash_free_fn free_fn)
{
	
	if (self && free_fn ) {
		zlist_t *keys = zhash_keys(self);
		
		if (keys) {
			char *key = zlist_first(keys);
			while (key) {
				zhash_freefn(self, key, free_fn);
				key = zlist_next(keys);
			}
			zlist_destroy(&keys);
		}
	}else {
		MDEBUG(MD_XF_GATEWAY,"tracking proxy something was NULL s[%p] h[%p] f[%p]\n",self,self,free_fn);
	}
}

/// initialize the xvalue manager
int
xhash_init(zhash_t *self, gwcfg_t *gwc)
{
	if (self) {
		s_init_item_hash(self, gwc);
		
		//printf("%s:%d got here\n",__FUNCTION__,__LINE__);
		return 0;
	}
	return -1;
}

/// Frees key if free_key==TRUE
/// Sets freefn if != NULL
int
xhash_insert_f(zhash_t *self, char *key, void *item, bool free_key, zhash_free_fn freefn)
{
	int test=-1;
	
	if(self){
		if (zhash_lookup(self, key)) {
			fprintf(stderr,"%s:%d - WARNING: adding an existing key [%s] - may indicate duplicate value in config file\n",__FUNCTION__,__LINE__,key);
			test=-1;
		}else {
			test=zhash_insert(self, key, item);
		}		

		if (freefn) {
			zhash_freefn(self, key, xvalue_free);
		}
		
		if (free_key) {
			free(key);
		}
	}
	return test;
}

/// Frees key
/// Sets freefn == xvalue_free
int
xhash_insert_v(zhash_t *self, char *key, xvalue_t *item)
{
	int test=xhash_insert_f(self, key, item, TRUE, xvalue_free);
	return test;
}

int
xhash_insert_cp(zhash_t *self, const char *key, void *item)
{
	char *kcp=strdup(key);
	return xhash_insert_f(self, kcp, item, TRUE, NULL);
}

bool
xhash_has_key(zhash_t *self, const char *key)
{
	if (self) {
		if (zhash_lookup(self, key)) {
			return TRUE;
		}
	}
	return FALSE;
}

zlist_t *
xhash_lkey_set(zlist_t *keys, const char *path, xhash_compare_fn compfn)
{
	if (keys == NULL || path==NULL || compfn==NULL) {
		return NULL;
	}
	
	if (keys) {
		zlist_t *key_set=zlist_new();
		zlist_autofree(key_set);
		
		char *key = (char *)zlist_first(keys);
		
		while (key) {
			if (compfn((void *)key, (void *)path)) {
				zlist_append(key_set, (void *)(key));
			}
			
			key=zlist_next(keys);
		}
		
		
		if (zlist_size(key_set) <= 0 ) {
			zlist_destroy(&key_set);
			return NULL;
		}
		return key_set;
	}
	return NULL;	
}

zlist_t *
xhash_key_set(zhash_t *self, const char *path)
{
	if (self) {
		zlist_t *keys=zhash_keys(self);
		zlist_t *key_set = NULL;
		if(path)
			key_set=xhash_lkey_set(keys, path, xhash_str_key_compare);
		else {
			key_set=zhash_keys(self);
		}
		
		zlist_destroy(&keys);
		return key_set;
	}
	return NULL;
}


// write (off_t)strlen+(char *)strbytes
// include NULL byte
// return pointer to serialized data 
// total number of bytes written
size_t
xhash_string_bwrite(void *self, byte **dest)
{
	size_t vret = 0;
	
	if (self && dest) {
		
		// allocate memory for size and data fields
		// (off_t len)(char[len])
		uint32_t mlen=sizeof(uint32_t);
		uint32_t slen=(self?strlen(self):0)+1;
		size_t len=mlen+slen;
		
		byte *bp = (byte *)malloc(len*sizeof(byte));
		if (bp==NULL) {
			printf("%s:%d - malloc error\n",__FUNCTION__,__LINE__);
			return -1;
		}
		// init pointers
		byte *ip=bp;
		len=0;
		// write string len
		// and adjust pointers
		memcpy(ip,&slen,sizeof(uint32_t));
		len+=sizeof(uint32_t);
		ip+=sizeof(uint32_t);
		
		// write string data
		// and adjust pointers
		if (slen>0) {
			memcpy(ip,self,slen);
			len+=slen;
			ip+=slen;
		}
		
		// set output values
		*dest = bp;
		vret=len;
		//printf("%s:%d - got here sz[%d] buf[%p]\n",__FUNCTION__,__LINE__,*sz,*buf);
	}else {
		vret = 0;
	}
	
	return vret;
}

// read new serialized string (len,string,NULL)
// at raw.
// return new string.
// if ptr != NULL, set to memory location
// following string
char *
xhash_string_bread(byte *raw, byte **ptr)
{
    char *new_obj =NULL;
    
    if(NULL!=raw){
        new_obj = (char *)malloc(sizeof(char));
        if(NULL!=new_obj){
            // point to raw bytes
            byte *iptr=raw;
            // read size
            uint32_t slen = (uint32_t) *(iptr);
            iptr+=sizeof(uint32_t);
            
            // read data
            if (slen>1) {
                char *op=new_obj;
                new_obj = (char *)realloc(new_obj,slen*sizeof(char));
                if(NULL!=new_obj){
                    memcpy(new_obj,iptr,slen);
                    iptr+=slen;
                }else{
                    // free the first alloc
                    free(op);
                }
            }else {
                new_obj[0]='\0';
                iptr+=1;
            }
            
            if (ptr) {
                // if ptr passed in,
                // point it at next memory location
                // this is to make it easy to read multiple items
                *ptr = iptr;
            }
       }
    }
	
	return new_obj;
}	

void *
xhash_string_vbread(byte *raw, byte **ptr)
{
	return (void *)xhash_string_bread(raw, ptr);
}

// return zhash with key:xvalue pairs
static zhash_t *
xhash_bread(byte *src, size_t size, xhash_serialr_fn serial_fn, xf_free_fn free_fn)
{
	zhash_t *hash=NULL;
	bool error=FALSE;
	
	if (src && serial_fn && size>0 ) {
		// point to start of data
		byte *iptr=src;
		// create the hash
		hash=zhash_new();
		void *val=NULL;
		
		while ((iptr-src)<size) {
			// read key size
			uint32_t ksize = (uint32_t)(*iptr);
			
			// point to start of key
			iptr+=sizeof(uint32_t);
			// read key
			char *key = (char *)(iptr);
			
			// point to item len
			iptr+=ksize;
			// read item len
//			uint32_t ilen = (uint32_t)(*iptr);
			
			// point to start of item data
			iptr+=sizeof(uint32_t);
			// create pointer 
			byte *idata=iptr;
			byte *next=NULL;
			//printf("%s:%d - reading item key[%s] klen[%d] ilen[%d]\n",__FUNCTION__, __LINE__, key,ksize,ilen);
			// read item data
			val = (void *)serial_fn(idata,&next);
			if (NULL!=val && NULL!=key) {

				// point to next item
				iptr=next;
				// put item in hash
				zhash_insert(hash, key, val);
				if (free_fn) {
					zhash_freefn(hash, key, free_fn);
				}
			}else {
				// bad bad bad
                fprintf(stderr, "%s:%d - read error for key [%s]\n",__FUNCTION__,__LINE__,(key==NULL?"NULL":key));
				error=TRUE;
				break;
			}
		}		
	}

	if (error) {
		if (hash) {
			zhash_destroy(&hash);
		}
		return NULL;
	}

	return hash;
}

// serialize entries in hash matching key_set
static size_t
xhash_bwrite(zhash_t *self, zlist_t *key_list, byte **dest, xhash_serialw_fn serial_fn)
{
	if(self==NULL || dest==NULL || serial_fn==NULL){
		fprintf(stderr,"%s:%d - bad argument error self[%p] kset[%p] dest[%p] sfn[%p]\n",__FUNCTION__,__LINE__,self,key_list,dest,serial_fn);
		return -1;
	}
	
	byte *dbuf   = NULL;
	byte *ibuf   = NULL;
	byte *op     = NULL;
	size_t olen  = 0;
	bool error   = FALSE;
	
	// if NULL keyset passed in, use all
	zlist_t *key_set=NULL;
	if (key_list==NULL) {
		key_set = zhash_keys(self);
	}else {
		key_set = key_list;
	}
	
	//printf("%s:%d - key_set[%p] dest[%p] \n",__FUNCTION__,__LINE__,key_set,dest);
	//printf("%s:%d - key_set_sz[%d]\n",__FUNCTION__,__LINE__,zlist_size(key_set));
	
	if (key_set && zlist_size(key_set)>0) {
		char *next_key = zlist_first(key_set);
		
		// for each key...
		while (next_key) {
			// get the item...
			void *item = zhash_lookup(self, next_key);
			if (item) {
                size_t isize=0;
				// serialize it using function provided
				// This sets the value of isize
				if ( (isize=serial_fn(item, &ibuf))>0) {
					// size the key
					size_t klen = strlen(next_key)+1;
					size_t ssz = sizeof(uint32_t);
					
					// add enough buffer space for 
					// the item and key					
					dbuf = (byte *)realloc(dbuf, (olen+ssz+klen+ssz+isize));
					
					//memset(dbuf+olen,0,(klen+ssz+isize));
					if( dbuf !=NULL){
						//printf("%s:%d - ibuf[%p] obuf[%p] op[%p] \n",__FUNCTION__,__LINE__,ibuf,dbuf,op);
						//printf("%s:%d - klen[%d] ssz[%d] olen[%d] isize[%d] sum[%d]\n",__FUNCTION__,__LINE__,klen,ssz,olen,isize,(olen+ssz+klen+ssz+isize));
						// point to the end
						op = dbuf+olen;
						
						// write size of key
						uint32_t sz_ui32=klen;
						memcpy(op,&sz_ui32,sizeof(uint32_t));
						op+=sizeof(uint32_t);
						olen+=sizeof(uint32_t);
						
						//write key
						memcpy(op,next_key,klen);
						op+=klen;
						olen+=klen;
						
						// write item len
						sz_ui32 = isize;
						memcpy(op,&sz_ui32,sizeof(uint32_t));
						op+=sizeof(uint32_t);
						olen+=sizeof(uint32_t);
						
						// write item
						memcpy(op,ibuf,isize);
						// write buffer len
						olen+=isize;
						
						// free item
						free(ibuf);
						// reset item buffer and size
						ibuf=NULL;
						isize = 0;
					}else {
						fprintf(stderr,"%s:%d - realloc error\n",__FUNCTION__,__LINE__);
						error=TRUE;
						break;
					}
				}else {
					fprintf(stderr,"%s:%d - serialize error\n",__FUNCTION__,__LINE__);
					error=TRUE;
					break;
				}
			}
			next_key=(char *)zlist_next(key_set);
		}
	}else {
		if(key_list==NULL){
			zlist_destroy(&key_set);
		}
		return 0;
	}
	
	if (error) {
		if(key_list==NULL){
			zlist_destroy(&key_set);
		}
		// something bad happened
		// deal with it
		if (dbuf) {
			free(dbuf);
		}
		if (ibuf) {
			free(ibuf);
		}
		return -1;
	}
	
	if(key_list==NULL){
		zlist_destroy(&key_set);
	}
	// release temp buffer
	if (ibuf) {
		free(ibuf);
	}
	
	// if destination is pointing
	// to something, free it
	// [should leave to caller]
	//if (*dest) {
	//	free(*dest);
	//}
	
	// point to new data
	*dest = dbuf;
	// return size
	return olen;
}

// de-serialize zhash with key:xvalue pairs
zhash_t *
xhash_xval_bytes2hash(byte *src, size_t size)
{
	if (src) {
		return xhash_bread(src, size, xvalue_vbread, xvalue_free);
	}
	return NULL;
}

// de-serialize zhash with key:string pairs
zhash_t *
xhash_str_bytes2hash(byte *src, size_t size)
{
	if (src) {
		return xhash_bread(src, size, xhash_string_vbread, free);
	}
	return NULL;
}


// serialize xhash containing key:xvalue pairs
size_t
xhash_xval_hash2bytes(zhash_t *self, byte **dest)
{
	if (self && dest) {
		size_t bsize=xhash_bwrite(self, NULL, dest, xvalue_bwrite);
		return bsize;
	}
	return 0;
}

// serialize xhash containing key:string pairs
size_t
xhash_str_hash2bytes(zhash_t *self, byte **dest)
{
	if (self && dest) {
		size_t bsize=xhash_bwrite(self, NULL, dest, xhash_string_bwrite);
		return bsize;
	}
	return 0;
}

/// Return key:value pairs as serialized 
/// xhash (key:xvalue pairs)
/// provide list of key,value parameters:
/// const char* key, xvalue_t *value,..
/// returns size of serialized hash 
/// buf points to serialized hash
size_t
xhash_vkxv_hash2bytes(byte **buf, ...)
{
	va_list ap;
	zhash_t *hash=zhash_new();
	const char *ikey=NULL;
	xvalue_t *value=NULL;
	bool error=FALSE;
	
	va_start(ap,buf);
	
	do{
		ikey=va_arg(ap, const char *);
		if (ikey==NULL) {
			break;
		}
		value=va_arg(ap, xvalue_t *);
		if (value==NULL) {
			error=TRUE;
			break;
		}
		zhash_insert(hash,ikey,value);
	}while(ikey!=NULL);
	
	va_end(ap);
	xhash_set_freefn(hash, xvalue_free);
	
	if (error) {
		zhash_destroy(&hash);
		return -1;
	}
	
	byte *bp=NULL;
	size_t sz=xhash_bwrite(hash,NULL, &bp, xvalue_bwrite);
	*buf = bp;
		
	// cleanup
	zhash_destroy(&hash);
	return sz;
}

void 
xhash_str_dumpfn(void *item, int indent)
{
	printf("%*s[str: %s]\n",indent,(indent?" ":""), (char *)item);
}

void 
xhash_idump(zhash_t *self, int indent, bool dump_items, xhash_dump_fn item_dump)
{

	indent=((indent>0 && indent<80)?indent:0);
	printf("%*s[hash: %p]\n",indent,(indent?" ":""), self);

	if (self) {
		
		printf("%*s[size: %lu]\n",indent,(indent?" ":""), (long unsigned)zhash_size(self));

		if (dump_items && item_dump) {
			zlist_t *keys = zhash_keys(self);
			if (keys) {
				zlist_sort(keys,s_key_compare);

				printf("%*s[keys:]\n",indent,(indent?" ":""));
				char *key = (char *)zlist_first(keys);				
				
				while (key) {
					void *item = zhash_lookup(self,key);
					printf("%*s[key: %s]\n",indent,(indent?" ":""), key);
					item_dump(item,indent);
					key= zlist_next(keys);
				}
			}else {
				fprintf(stderr,"%s:%d - keys are NULL/n",__FUNCTION__,__LINE__);
			}

			zlist_destroy(&keys);
		}
	}
	
	return;
}

void 
xhash_dump(zhash_t *self)
{
	xhash_idump(self,0,FALSE,NULL);
}

int 
xhash_test(bool verbose)
{
	// create a hash
	zhash_t *hash = zhash_new();

	char *key=NULL;
	zlist_t *keys=NULL;
	
	// initialize from default gateway config
	xhash_init(hash, NULL);

	// show it
	xhash_idump(hash,5,TRUE, xvalue_idumpw);
	
	printf("Showing /platform/debug keys:\n");	
	// get a key_set (subset of hash keys)
	keys = xhash_key_set(hash, "/platform/config/debug");
	// print them
	key = (char *)(keys?zlist_first(keys):NULL);
	if (keys) {
		while (key) {
			printf("     [key: %s]\n",key);
			xvalue_t *xv = (xvalue_t *)zhash_lookup(hash,key);
			xvalue_idump(xv,5);
			key=zlist_next(keys);
		}
	}
	
	// cleanup 
	zlist_destroy(&keys);
	zhash_destroy(&hash);

	//////
	// create a new hash
	hash = zhash_new();
	
	// put something in
	zhash_insert(hash, "/foo/bar/baz", xvalue_new_string("penny_whistle"));
	
	printf("%s:%d - before serialization:\n",__FUNCTION__,__LINE__);	
	xhash_idump(hash,5,TRUE, xvalue_idumpw);
	
	// serialize the hash
	byte *hdata = NULL;
	size_t hsz = xhash_xval_hash2bytes(hash, &hdata);
	
	// destroy the original
	xhash_set_freefn(hash,xvalue_free);
	zhash_destroy(&hash);
	
	printf("%s:%d - deserializing\n",__FUNCTION__,__LINE__);

	// deserialize the hash
	hash = xhash_xval_bytes2hash(hdata, hsz);

	printf("%s:%d - after serialization\n",__FUNCTION__,__LINE__);
	// show it
	xhash_idump(hash,5,TRUE, xvalue_idumpw);
	
	// clean up
	xhash_set_freefn(hash,xvalue_free);
	zhash_destroy(&hash);
	free(hdata);
	
	return 0;
}

