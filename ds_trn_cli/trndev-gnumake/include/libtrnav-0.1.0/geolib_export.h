
#ifndef GEOLIB_EXPORT_H
#define GEOLIB_EXPORT_H

#ifdef GEOLIB_STATIC_DEFINE
#  define GEOLIB_EXPORT
#  define GEOLIB_NO_EXPORT
#else
#  ifndef GEOLIB_EXPORT
#    ifdef geolib_EXPORTS
        /* We are building this library */
#      define GEOLIB_EXPORT 
#    else
        /* We are using this library */
#      define GEOLIB_EXPORT 
#    endif
#  endif

#  ifndef GEOLIB_NO_EXPORT
#    define GEOLIB_NO_EXPORT 
#  endif
#endif

#ifndef GEOLIB_DEPRECATED
#  define GEOLIB_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef GEOLIB_DEPRECATED_EXPORT
#  define GEOLIB_DEPRECATED_EXPORT GEOLIB_EXPORT GEOLIB_DEPRECATED
#endif

#ifndef GEOLIB_DEPRECATED_NO_EXPORT
#  define GEOLIB_DEPRECATED_NO_EXPORT GEOLIB_NO_EXPORT GEOLIB_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef GEOLIB_NO_DEPRECATED
#    define GEOLIB_NO_DEPRECATED
#  endif
#endif

#endif /* GEOLIB_EXPORT_H */
