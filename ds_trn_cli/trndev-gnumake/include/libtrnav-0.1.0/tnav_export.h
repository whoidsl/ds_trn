
#ifndef TNAV_EXPORT_H
#define TNAV_EXPORT_H

#ifdef TNAV_STATIC_DEFINE
#  define TNAV_EXPORT
#  define TNAV_NO_EXPORT
#else
#  ifndef TNAV_EXPORT
#    ifdef tnav_EXPORTS
        /* We are building this library */
#      define TNAV_EXPORT 
#    else
        /* We are using this library */
#      define TNAV_EXPORT 
#    endif
#  endif

#  ifndef TNAV_NO_EXPORT
#    define TNAV_NO_EXPORT 
#  endif
#endif

#ifndef TNAV_DEPRECATED
#  define TNAV_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef TNAV_DEPRECATED_EXPORT
#  define TNAV_DEPRECATED_EXPORT TNAV_EXPORT TNAV_DEPRECATED
#endif

#ifndef TNAV_DEPRECATED_NO_EXPORT
#  define TNAV_DEPRECATED_NO_EXPORT TNAV_NO_EXPORT TNAV_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef TNAV_NO_DEPRECATED
#    define TNAV_NO_DEPRECATED
#  endif
#endif

#endif /* TNAV_EXPORT_H */
