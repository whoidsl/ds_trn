
#ifndef TRNUCLI_EXPORT_H
#define TRNUCLI_EXPORT_H

#ifdef TRNUCLI_STATIC_DEFINE
#  define TRNUCLI_EXPORT
#  define TRNUCLI_NO_EXPORT
#else
#  ifndef TRNUCLI_EXPORT
#    ifdef trnucli_EXPORTS
        /* We are building this library */
#      define TRNUCLI_EXPORT 
#    else
        /* We are using this library */
#      define TRNUCLI_EXPORT 
#    endif
#  endif

#  ifndef TRNUCLI_NO_EXPORT
#    define TRNUCLI_NO_EXPORT 
#  endif
#endif

#ifndef TRNUCLI_DEPRECATED
#  define TRNUCLI_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef TRNUCLI_DEPRECATED_EXPORT
#  define TRNUCLI_DEPRECATED_EXPORT TRNUCLI_EXPORT TRNUCLI_DEPRECATED
#endif

#ifndef TRNUCLI_DEPRECATED_NO_EXPORT
#  define TRNUCLI_DEPRECATED_NO_EXPORT TRNUCLI_NO_EXPORT TRNUCLI_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef TRNUCLI_NO_DEPRECATED
#    define TRNUCLI_NO_DEPRECATED
#  endif
#endif

#endif /* TRNUCLI_EXPORT_H */
