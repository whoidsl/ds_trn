
#ifndef NEWMAT_EXPORT_H
#define NEWMAT_EXPORT_H

#ifdef NEWMAT_STATIC_DEFINE
#  define NEWMAT_EXPORT
#  define NEWMAT_NO_EXPORT
#else
#  ifndef NEWMAT_EXPORT
#    ifdef newmat_EXPORTS
        /* We are building this library */
#      define NEWMAT_EXPORT 
#    else
        /* We are using this library */
#      define NEWMAT_EXPORT 
#    endif
#  endif

#  ifndef NEWMAT_NO_EXPORT
#    define NEWMAT_NO_EXPORT 
#  endif
#endif

#ifndef NEWMAT_DEPRECATED
#  define NEWMAT_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef NEWMAT_DEPRECATED_EXPORT
#  define NEWMAT_DEPRECATED_EXPORT NEWMAT_EXPORT NEWMAT_DEPRECATED
#endif

#ifndef NEWMAT_DEPRECATED_NO_EXPORT
#  define NEWMAT_DEPRECATED_NO_EXPORT NEWMAT_NO_EXPORT NEWMAT_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef NEWMAT_NO_DEPRECATED
#    define NEWMAT_NO_DEPRECATED
#  endif
#endif

#endif /* NEWMAT_EXPORT_H */
