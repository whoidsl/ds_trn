
#ifndef QNX_EXPORT_H
#define QNX_EXPORT_H

#ifdef QNX_STATIC_DEFINE
#  define QNX_EXPORT
#  define QNX_NO_EXPORT
#else
#  ifndef QNX_EXPORT
#    ifdef qnx_EXPORTS
        /* We are building this library */
#      define QNX_EXPORT 
#    else
        /* We are using this library */
#      define QNX_EXPORT 
#    endif
#  endif

#  ifndef QNX_NO_EXPORT
#    define QNX_NO_EXPORT 
#  endif
#endif

#ifndef QNX_DEPRECATED
#  define QNX_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef QNX_DEPRECATED_EXPORT
#  define QNX_DEPRECATED_EXPORT QNX_EXPORT QNX_DEPRECATED
#endif

#ifndef QNX_DEPRECATED_NO_EXPORT
#  define QNX_DEPRECATED_NO_EXPORT QNX_NO_EXPORT QNX_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef QNX_NO_DEPRECATED
#    define QNX_NO_DEPRECATED
#  endif
#endif

#endif /* QNX_EXPORT_H */
