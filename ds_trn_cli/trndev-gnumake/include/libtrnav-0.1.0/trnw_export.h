
#ifndef TRNW_EXPORT_H
#define TRNW_EXPORT_H

#ifdef TRNW_STATIC_DEFINE
#  define TRNW_EXPORT
#  define TRNW_NO_EXPORT
#else
#  ifndef TRNW_EXPORT
#    ifdef trnw_EXPORTS
        /* We are building this library */
#      define TRNW_EXPORT 
#    else
        /* We are using this library */
#      define TRNW_EXPORT 
#    endif
#  endif

#  ifndef TRNW_NO_EXPORT
#    define TRNW_NO_EXPORT 
#  endif
#endif

#ifndef TRNW_DEPRECATED
#  define TRNW_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef TRNW_DEPRECATED_EXPORT
#  define TRNW_DEPRECATED_EXPORT TRNW_EXPORT TRNW_DEPRECATED
#endif

#ifndef TRNW_DEPRECATED_NO_EXPORT
#  define TRNW_DEPRECATED_NO_EXPORT TRNW_NO_EXPORT TRNW_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef TRNW_NO_DEPRECATED
#    define TRNW_NO_DEPRECATED
#  endif
#endif

#endif /* TRNW_EXPORT_H */
