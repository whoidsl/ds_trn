
#ifndef MFRAME_EXPORT_H
#define MFRAME_EXPORT_H

#ifdef MFRAME_STATIC_DEFINE
#  define MFRAME_EXPORT
#  define MFRAME_NO_EXPORT
#else
#  ifndef MFRAME_EXPORT
#    ifdef mframe_EXPORTS
        /* We are building this library */
#      define MFRAME_EXPORT 
#    else
        /* We are using this library */
#      define MFRAME_EXPORT 
#    endif
#  endif

#  ifndef MFRAME_NO_EXPORT
#    define MFRAME_NO_EXPORT 
#  endif
#endif

#ifndef MFRAME_DEPRECATED
#  define MFRAME_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef MFRAME_DEPRECATED_EXPORT
#  define MFRAME_DEPRECATED_EXPORT MFRAME_EXPORT MFRAME_DEPRECATED
#endif

#ifndef MFRAME_DEPRECATED_NO_EXPORT
#  define MFRAME_DEPRECATED_NO_EXPORT MFRAME_NO_EXPORT MFRAME_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef MFRAME_NO_DEPRECATED
#    define MFRAME_NO_DEPRECATED
#  endif
#endif

#endif /* MFRAME_EXPORT_H */
