/* LCM type definition class file
 * This file was automatically generated by lcm-gen
 * DO NOT MODIFY BY HAND!!!!
 */

package trn;
 
import java.io.*;
import java.util.*;
import lcm.lcm.*;
 
public final class trnupub_t implements lcm.lcm.LCMEncodable
{
    public int sync;
    public trn.trnu_estimate_t est[];
    public int reinit_count;
    public double reinit_tlast;
    public int filter_state;
    public int success;
    public short is_converged;
    public short is_valid;
    public int mb1_cycle;
    public int ping_number;
    public int n_con_seq;
    public int n_con_tot;
    public int n_uncon_seq;
    public int n_uncon_tot;
    public double mb1_time;
    public double reinit_time;
    public double update_time;
 
    public trnupub_t()
    {
        est = new trn.trnu_estimate_t[5];
    }
 
    public static final long LCM_FINGERPRINT;
    public static final long LCM_FINGERPRINT_BASE = 0xc559aa4053a9883dL;
 
    static {
        LCM_FINGERPRINT = _hashRecursive(new ArrayList<Class<?>>());
    }
 
    public static long _hashRecursive(ArrayList<Class<?>> classes)
    {
        if (classes.contains(trn.trnupub_t.class))
            return 0L;
 
        classes.add(trn.trnupub_t.class);
        long hash = LCM_FINGERPRINT_BASE
             + trn.trnu_estimate_t._hashRecursive(classes)
            ;
        classes.remove(classes.size() - 1);
        return (hash<<1) + ((hash>>63)&1);
    }
 
    public void encode(DataOutput outs) throws IOException
    {
        outs.writeLong(LCM_FINGERPRINT);
        _encodeRecursive(outs);
    }
 
    public void _encodeRecursive(DataOutput outs) throws IOException
    {
        outs.writeInt(this.sync); 
 
        for (int a = 0; a < 5; a++) {
            this.est[a]._encodeRecursive(outs); 
        }
 
        outs.writeInt(this.reinit_count); 
 
        outs.writeDouble(this.reinit_tlast); 
 
        outs.writeInt(this.filter_state); 
 
        outs.writeInt(this.success); 
 
        outs.writeShort(this.is_converged); 
 
        outs.writeShort(this.is_valid); 
 
        outs.writeInt(this.mb1_cycle); 
 
        outs.writeInt(this.ping_number); 
 
        outs.writeInt(this.n_con_seq); 
 
        outs.writeInt(this.n_con_tot); 
 
        outs.writeInt(this.n_uncon_seq); 
 
        outs.writeInt(this.n_uncon_tot); 
 
        outs.writeDouble(this.mb1_time); 
 
        outs.writeDouble(this.reinit_time); 
 
        outs.writeDouble(this.update_time); 
 
    }
 
    public trnupub_t(byte[] data) throws IOException
    {
        this(new LCMDataInputStream(data));
    }
 
    public trnupub_t(DataInput ins) throws IOException
    {
        if (ins.readLong() != LCM_FINGERPRINT)
            throw new IOException("LCM Decode error: bad fingerprint");
 
        _decodeRecursive(ins);
    }
 
    public static trn.trnupub_t _decodeRecursiveFactory(DataInput ins) throws IOException
    {
        trn.trnupub_t o = new trn.trnupub_t();
        o._decodeRecursive(ins);
        return o;
    }
 
    public void _decodeRecursive(DataInput ins) throws IOException
    {
        this.sync = ins.readInt();
 
        this.est = new trn.trnu_estimate_t[(int) 5];
        for (int a = 0; a < 5; a++) {
            this.est[a] = trn.trnu_estimate_t._decodeRecursiveFactory(ins);
        }
 
        this.reinit_count = ins.readInt();
 
        this.reinit_tlast = ins.readDouble();
 
        this.filter_state = ins.readInt();
 
        this.success = ins.readInt();
 
        this.is_converged = ins.readShort();
 
        this.is_valid = ins.readShort();
 
        this.mb1_cycle = ins.readInt();
 
        this.ping_number = ins.readInt();
 
        this.n_con_seq = ins.readInt();
 
        this.n_con_tot = ins.readInt();
 
        this.n_uncon_seq = ins.readInt();
 
        this.n_uncon_tot = ins.readInt();
 
        this.mb1_time = ins.readDouble();
 
        this.reinit_time = ins.readDouble();
 
        this.update_time = ins.readDouble();
 
    }
 
    public trn.trnupub_t copy()
    {
        trn.trnupub_t outobj = new trn.trnupub_t();
        outobj.sync = this.sync;
 
        outobj.est = new trn.trnu_estimate_t[(int) 5];
        for (int a = 0; a < 5; a++) {
            outobj.est[a] = this.est[a].copy();
        }
 
        outobj.reinit_count = this.reinit_count;
 
        outobj.reinit_tlast = this.reinit_tlast;
 
        outobj.filter_state = this.filter_state;
 
        outobj.success = this.success;
 
        outobj.is_converged = this.is_converged;
 
        outobj.is_valid = this.is_valid;
 
        outobj.mb1_cycle = this.mb1_cycle;
 
        outobj.ping_number = this.ping_number;
 
        outobj.n_con_seq = this.n_con_seq;
 
        outobj.n_con_tot = this.n_con_tot;
 
        outobj.n_uncon_seq = this.n_uncon_seq;
 
        outobj.n_uncon_tot = this.n_uncon_tot;
 
        outobj.mb1_time = this.mb1_time;
 
        outobj.reinit_time = this.reinit_time;
 
        outobj.update_time = this.update_time;
 
        return outobj;
    }
 
}

