
/// @file app_frame.hpp
/// @authors k. headley
/// @date 21mar2022

/// Summary: application frame for command line apps
// ///////////////////////
// Copyright 2022  Monterey Bay Aquarium Research Institute
// Distributed under MIT license. See LICENSE file for more information.

// /////////////////
// Includes
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <getopt.h>
// initalizers for header-only modules
// must be defined before any framework headers are included
#define INIT_PCF_LOG
#define INIT_TRN_LCM_INPUT
#include "lcm_interface.hpp"
#include "lcm_pcf/signal_t.hpp"
#include "trn_lcm_input.hpp"
#include "trnxpp.hpp"

// /////////////////
// Macros
/// @def VERSION_HELPER
/// @brief version string helper.
#define VERSION_HELPER(s) #s
/// @def VERSION_STRING
/// @brief version string macro.
#define VERSION_STRING(s) VERSION_HELPER(s)

#define TRNXPP_NAME "trnxpp"
#ifndef TRNXPP_BUILD
/// @def TRNXPP_BUILD
/// @brief module build date.
/// Sourced from CFLAGS in Makefile
/// w/ -DMBTRN_BUILD=`date`
#define TRNXPP_BUILD "" VERSION_STRING(APP_BUILD)
#endif
#ifndef TRNXPP_VERSION
/// @def TRNXPP_BUILD
/// @brief module build date.
/// Sourced from CFLAGS in Makefile
/// w/ -DMBTRN_BUILD=`date`
#define TRNXPP_VERSION "" VERSION_STRING(TRNXPP_VER)
#endif

/// @def WIN_DECLSPEC
/// @brief declaration for windows.
//#ifdef _WIN64
////define something for Windows (64-bit only)
//#       define WIN_DECLSPEC __declspec(dllimport)
//#else // is WIN32
////define something for Windows (32-bit only)
//#       define WIN_DECLSPEC __declspec(dllimport)
//#endif // WIN64

#if defined(__CYGWIN__)

/// @def WIN_DECLSPEC
/// @brief declaration for windows.
#define WIN_DECLSPEC __declspec(dllimport)
#else
#define WIN_DECLSPEC
#endif

class app_frame{
public:
    app_frame()
    : verbose(false), debug(false)
    {
    }

    ~app_frame()
    {

    }

    parse_args(int argc, char **argv){

    }

    void show_help()
    {

    }
protected:
private:
    bool mVerbose;
    bool mDebug;
    std::list<std::string> mInputList;
};
