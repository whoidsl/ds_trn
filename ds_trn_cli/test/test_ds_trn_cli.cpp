/**
 * Copyright 2022 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by tmjoyce
//

#include <ds_trn_cli/ds_trn_cli.h>
#include <gtest/gtest.h>

namespace ds_trn {

//////////////////////////////////////////////////////////
class TrnForwardingTest : public ::testing::Test {

protected:
  ds_trn::TrnCli *node = new ds_trn::TrnCli(false);
};

TEST_F(TrnForwardingTest, test_shift_maximum_allowed) {

  // Note max dist and calc point sign are loaded into ros param server through
  // a yaml
  // this testing functionality currently has no way to grab those values, so it
  // uses the defaults defined in the class.
  // You would need to run a dummy param server or fully initialize the classes
  // param server
  // to have those vals consistent.

  double x1, y1, x2, y2, max_dist = 5;
  // lets use perfect right triangles for easy testing
  x2 = 3;
  y2 = 4;

  // x testing
  x1 = 0;
  y1 = 0;

  ASSERT_EQ(node->calculatePointDistance(x1, y1, x2, y2), max_dist);

  x1 = -3;
  y1 = -4;
  if (node->getSwapCalcPointSign() >= 1) {
    ASSERT_EQ(node->calculatePointDistance(x1, y1, x2, y2), 0);
  } else {
    ASSERT_EQ(node->calculatePointDistance(x1, y1, x2, y2), max_dist * 2);
  }
}

} // namespace ds_trn

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  ros::Time::init();
  //ros::init(argc, argv, "ds_trn_cli_test_init");

  //ros::NodeHandle nh;
  return RUN_ALL_TESTS();
}
