/**
 * Copyright 2018 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef DS_TRN_CLI_H
#define DS_TRN_CLI_H

// WHOI Headers
#include "ds_base/ds_process.h"
#include "ds_core_msgs/RawData.h"
#include "ds_base/sensor_base.h"
#include <ds_base/util.h>
#include "ros/ros.h"
#include "std_msgs/String.h"
#include "ds_core_msgs/RawData.h"

// MBARI Headers
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#ifdef __cplusplus
extern "C" {
#endif

#include "trnu_cli.h"
#ifdef __cplusplus
}
#endif
#include "mframe.h"
#include "mfile.h"
#include "msocket.h"
#include "mlog.h"
#include "mtime.h"
#include "medebug.h"
#include "trn_msg.h"
#include "ds_trn_cli/TrnUpdate.h"
#include "ds_trn_cli/ResetTRNOffsetCmd.h"
#include "ds_trn_cli/ResetTRNCmd.h"
#include "ds_trn_cli/ConDisconCmd.h"
#include "ds_trn_cli/ShiftForwardingCmd.h"

// System Headers
#include <vector>
#include <string>
#include <sstream>
#include <cstdlib>
#include <cmath>

typedef enum {
  SRC_CSV=0,
  SRC_TRNU,
  SRC_BIN
}trnucli_src_type;

typedef struct app_cfg_s{
    /// @var app_cfg_s::verbose
    /// @brief TBD
    bool verbose;
    /// @var app_cfg_s::ifile
    /// @brief TBD
    char *ifile;
    /// @var app_cfg_s::input_src
    /// @brief TBD
    trnucli_src_type input_src;
    /// @var app_cfg_s::trnu_host
    /// @brief TBD
    char *trnu_host;
    /// @var app_cfg_s::trnu_port
    /// @brief TBD
    int trnu_port;
    /// @var app_cfg_s::trnu_ttl
    /// @brief TBD
    int trnu_ttl;
    /// @var app_cfg_s::trnu_hbeat
    /// @brief TBD
    int trnu_hbeat;
    /// @var app_cfg_s::hbeat_to_sec
    /// @brief TBD
    double hbeat_to_sec;
    /// @var app_cfg_s::flags
    /// @brief TBD
    trnucli_flags_t cli_flags;
    /// @var app_cfg_s::flags
    /// @brief TBD
    trnuctx_flags_t ctx_flags;
    /// @var app_cfg_s::update_n
    /// @brief TBD
    int update_n;
    /// @var app_cfg_s::ofmt
    /// @brief TBD
    trnuc_fmt_t ofmt;
    /// @var app_cfg_s::ofile
    /// @brief TBD
    FILE *ofile;
    /// @var app_cfg_s::demo
    /// @brief TBD
    int demo;
    /// @var app_cfg_s::test_reset_mod
    /// @brief TBD
    int test_reset_mod;
    /// @var app_cfg_s::async
    /// @brief TBD
    uint32_t async;
    /// @var app_cfg_s::session_timer
    /// @brief TBD
    double session_timer;
    /// @var app_cfg_s::recon_timer
    /// @brief TBD
    double recon_timer;
    /// @var app_cfg_s::recon
    /// @brief TBD
    double recon_to_sec;
    /// @var app_cfg_s::stats_log_period_sec
    /// @brief TBD
    double stats_log_period_sec;
    /// @var app_cfg_s::listen_to_msec
    /// @brief TBD
    uint32_t listen_to_ms;
    /// @var app_cfg_s::nddelms
    /// @brief TBD
    uint32_t enodata_delay_ms;
    /// @var app_cfg_s::rcdelms
    /// @brief TBD
    uint32_t erecon_delay_ms;
    /// @var app_cfg_s::log_cfg
    /// @brief TBD
    mlog_config_t *log_cfg;
    /// @var app_cfg_s::log_id
    /// @brief TBD
    mlog_id_t log_id;
    /// @var app_cfg_s::log_name
    /// @brief TBD
    char *log_name;
    /// @var app_cfg_s::log_dir
    /// @brief TBD
    char *log_dir;
    /// @var app_cfg_s::log_path
    /// @brief TBD
    char *log_path;
}app_cfg_t;

#define TRNUCLI_TEST_TRNU_PORT 8000
#define TRNUCLI_TEST_TRNU_HBEAT 25
#define TRNUCLI_TEST_CSV_LINE_BYTES 1024*20
#define TRNUCLI_TEST_UPDATE_N 10
#define TRNUCLI_TEST_LOG_NAME "trnucli"
#define TRNUCLI_TEST_LOG_DESC "trnu client log"
#define TRNUCLI_TEST_LOG_DIR  "."
#define TRNUCLI_TEST_LOG_EXT  ".log"
#define TRNUCLI_TEST_CMD_LINE_BYTES 2048
#define TRNUCLI_TEST_CONNECT_WAIT_SEC 5
#define TRNUCLI_TEST_ELISTEN_RETRIES 5
#define TRNUCLI_TEST_ELISTEN_WAIT 3
#define TRNUCLI_TEST_ENODATA_DELAY_MSEC  50
#define TRNUCLI_TEST_ERECON_DELAY_MSEC  5000
#define TRNUCLI_TEST_RECON_TMOUT_SEC 10.0
#define TRNUCLI_TEST_HBEAT_TMOUT_SEC 0.0
#define TRNUCLI_TEST_LISTEN_TMOUT_MSEC 50
#define TRNUCLI_TEST_LOG_EN CTX_LOG_EN
#define TRNUCLI_TEST_STATS_LOG_PERIOD_SEC 60.0
#define TRNUCLI_TEST_OFILE stdout
#define TRNUCLI_TEST_OFMT TRNUC_FMT_PRETTY
#define TRNUCLI_TEST_SRC SRC_TRNU

#define TIMEOUT_DECREMENT 1.0

namespace ds_trn
{
  class TrnCli : public ds_base::DsProcess
  {

  public:
    TrnCli(bool start_client=true);
    TrnCli(int argc, char* argv[], const std::string& name);
    ~TrnCli() override;

    double calculatePointDistance(double x1, double y1,
                                   double x2, double y2);
    int getSwapCalcPointSign();

  protected:
    void setupSubscriptions() override;
    void setupConnections() override;
    void setupPublishers() override;
    void setupParameters() override;
    void setupServices() override;
    void setupTimers() override;

    void initializeClient();
    void startClient();
    void shutdownClient();
    void publishTRNUpdate(trnu_pub_t *update);

    void onMcaJtrosMsg(const std_msgs::String::ConstPtr msg);

    void timeout_updater(const ros::TimerEvent &);

    void onExtData(ds_core_msgs::RawData bytes);

    static int trnu_update_callback(trnu_pub_t *update)
    {
      //need to be able to reset the filter on command (they have an API call for that- it’s likely to be a service, might not be depending on time to reset )
      ROS_INFO_STREAM("filter state is " << update->filter_state << " mb1_time: " << std::fixed << update->mb1_time << " update_time: " << update->update_time << std::endl);
      //need to be able to get the navigation estimate 
      // Time to publish the TRN Update!
      self_handle->publishTRNUpdate(update);

      return 0;
    }

  private:

    // These store the last values and time of user shift
    ros::Time last_user_shift_time;
    int       last_user_shift_easting;
    int       last_user_shift_northing;

    // for service advertisement
    ros::ServiceServer reset_service;
    ros::ServiceServer reset_offset_service;
    ros::ServiceServer con_discon_service;
    ros::ServiceServer shift_forwarding_service;

    // Handles for publisher/subscriber handles
    ros::Publisher m_pub;
    // Use the following publisher for sending new shftabs to mc
    ros::Publisher mca_shftabs_pub_;
    // use the following subscriber for receiving user shftabs (acoustically for example)
    ros::Subscriber mca_shftabs_sub_;

    
    // TRN client handle
    trnucli_ctx_t *ctx;
    // TRN client handle's configuration object
    app_cfg_t *cfg;

    // Declaring a holder variable for "this"
    static TrnCli *self_handle;

    // shift forwarding timeout parameters and timer handle
    bool active_timer;
    int shift_time;
    ros::Timer timeout_timer;

    bool reset_TRN(ds_trn_cli::ResetTRNCmd::Request& req, ds_trn_cli::ResetTRNCmd::Response& resp);
    bool reset_TRN_ofs_trn(ds_trn_cli::ResetTRNOffsetCmd::Request& req, ds_trn_cli::ResetTRNOffsetCmd::Response& resp);
    bool con_discon_TRN(ds_trn_cli::ConDisconCmd::Request& req, ds_trn_cli::ConDisconCmd::Response& resp);
    bool control_shift_forwarding(ds_trn_cli::ShiftForwardingCmd::Request& req, ds_trn_cli::ShiftForwardingCmd::Response& resp);

    // UDP connection to external
    boost::shared_ptr<ds_asio::DsConnection> ext_conn_;

    int shift_mult_factor_;
    int swap_trn_xy_;

    int enable_forwarding_; // if this is 1, it actually forwards shift data to mc
    
    // The actual command going out to MC
    std::string cmd_string;

    // max shift northing/easting magnitude diff in meters. Defaults to val in
    // constructor before getting from param server
    double max_shift_difference;
    //Swap the x2, and y2 signs when calculating 2d point distance if this is 1 
    int swap_point_calc_sign;
  };
}

#endif

