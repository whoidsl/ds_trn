#!/bin/bash

# Build the trndev packages as described in README-trndev-build.md

if [ "$#" != "1" ]; then
    echo "Usage: ./trndev-build.sh <absolute path to cmake binary>"
    echo "Latest tested cmake-3.18.0"
    exit -1
fi

if [ ! -d mframe ]
then
echo " missing mframe directory"
exit -1
fi

if [ ! -d libtrnav ]
then
echo " missing libtrnav directory"
exit -1
fi

cmake_bin=$1

CMAKE=$(which cmake)

TRNDEV_TOP=$PWD

TRNDEV_INSTALL=${TRNDEV_INSTALL:-${TRNDEV_TOP}/install}

build_cmake_pkg(){
    echo "Using $1 cmake binary"
    mkdir -p build
    cd build
    $cmake_bin ..
    $cmake_bin --build .
    $cmake_bin --install . --prefix `pwd`/pkg
    if [ ! -d ${TRNDEV_INSTALL} ]
    then
        mkdir -p ${TRNDEV_INSTALL}
    fi
    echo "installing to ${TRNDEV_INSTALL}"
    $cmake_bin --install . --prefix ${TRNDEV_INSTALL}
}

cd ${TRNDEV_TOP}/mframe
build_cmake_pkg

cd ${TRNDEV_TOP}/libtrnav
build_cmake_pkg

