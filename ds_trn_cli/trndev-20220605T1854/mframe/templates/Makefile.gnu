#############################################################################
# build environment

# version and build configuration

# uncomment and optionally define on command line
# to override value defined in source, e.g.
#  MYAPP_VER=1.0.2b0 make
MYAPP_VER ?=0.0.0b0
MYAPP_BUILD ?=`date +%Y/%m/%dT%H:%M:%S%z`
OPT_MYAPP_VER=-DLIN_APP_VER="$(MYAPP_VER)"
OPT_MYAPP_BUILD=-DLIN_APP_BUILD="$(MYAPP_BUILD)"
BUILD_OPTS=

# select MY_BUILD_OPT
# [use WITH_MY_BUILD_OPT=1 make...]
ifdef WITH_MY_BUILD_OPT
BUILD_OPTS+= -DWITH_MY_BUILD_OPT
endif

# user symbols
# C standard e.g. -std=c99 -std=gnu99
# may be needed for Cygwin (e.g. for loop declare/init)
# not currently C89 compatible
STD= #-std=c99 -std=gnu99 -std=c89
OPTIONS= $(STD) -D_GNU_SOURCE $(BUILD_OPTS) $(OPT_MYAPP_VER) $(OPT_MYAPP_BUILD)

# dependency paths

# build directories
OUTPUT_DIR=./bin
BUILD_DIR=./build

# compilation
CXX = gcc
# compile and link with -pg for profiling support
# then do gprof <application> gmon.out for profile output to console
GPROF= #-pg # for gprofile support
CFLAGS = -g -Wall $(GPROF) -Wno-deprecated -Wsign-compare -O2 $(OPTIONS)
LD_FLAGS = $(GPROF) -g -lm
INC_PATHS =  -I../../src/controller/system
LIB_PATHS = -L$(OUTPUT_DIR)

#############################################################################
# target definitions

MYAPP=app_name
MYAPP_SRC=app_name.c
MYAPP_OBJ=$(MYAPP_SRC:%.c=$(BUILD_DIR)/%.o)
MYAPP_LIBS =

# doxyen: Doxygen documentation

#############################################################################
# build files (mostly for cleanup)
SOURCES = $(MYAPP_SRC)
OBJECTS = $(SOURCES:%.c=$(BUILD_DIR)/%.o)
DEPENDS = $(SOURCES:%.c=$(BUILD_DIR)/%.d)
BINARIES = $(OUTPUT_DIR)/$(MYAPP)
DSYMS   = $(BINARIES:%=%.dSYM)
CLEANUP = gmon.out

#############################################################################
# rules: build targets

all: $(OBJECTS) $(OUTPUT_DIR)/$(MYAPP)

help:
	@echo ""
	@echo " Build targets:"
	@echo "  all   : build linspect  [1]"
	@echo "  clean : delete build products"
	@echo "  purge : delete build products, build and output directories"
	@echo "  help  : this help message"
	@echo ""
	@echo " Environment variables:"
	@echo "  MYAPP_VER         : set app version"
	@echo "  MYAPP_BUILD       : set app build ID"
	@echo "  WITH_MY_BUILD_OPT : set build option"
	@echo ""
	@echo "  defaults: "
	@echo "   MYAPP_VER [0.0.0b0] MYAPP_BUILD [iso8601_time]"
	@echo "   MY_BUILD_OPT [?]"
	@echo ""
	@echo "  example:"
	@echo "   MYAPP_VER=1.2.0b0 make clean all"
	@echo ""
	@echo " Notes:"
	@echo "  [1] output: binaries->bin/ objects->build/"
	@echo ""

# build linspect
$(OUTPUT_DIR)/$(MYAPP): $(MYAPP_OBJ)
	@echo building $@...
	$(CXX) $(INC_PATHS) $(OPTIONS) $(LIB_PATHS) $^ -o $@ $(MYAPP_LIBS) $(LD_FLAGS)
	@echo

# rule: build object files from source files
$(BUILD_DIR)/%.o :%.c
	@echo compiling $<...
	$(CXX) $(CFLAGS) $(INC_PATHS) -c $< -o $@
	@echo

# rule: build dependency files from source files
$(BUILD_DIR)/%.d :%.c
	@[ -d $(BUILD_DIR) ] || mkdir -p $(BUILD_DIR)
	@[ -d $(OUTPUT_DIR) ] || mkdir -p $(OUTPUT_DIR)
	@echo generating dependency file for $<
	@set -e; $(CXX) -MM $(CFLAGS) $(INC_PATHS) $< \
	| awk '/o:/ {printf "%s", "$@ $(BUILD_DIR)/"} {print}' > $@; \
	[ -s $@ ] || rm -f $@
	@echo

install:
	@echo "Installing...(not implemented)"


###########################################################################
# rules:

.PHONY: clean
.PHONY: purge

# clean : delete object, dependency, binary files
clean:
	rm -f $(OBJECTS) $(DEPENDS) $(BINARIES)
	rm -rf $(DSYMS)

# purge : delete delete object, dependency, binary files, build directories
purge:
	rm -f $(BINARIES) $(OBJECTS) $(DEPENDS) $(DSYMS) $(CLEANUP)
	rm -rf $(OUTPUT_DIR) $(BUILD_DIR)

# include the dependencies
ifneq ($(MAKECMDGOALS),purge)
ifneq ($(MAKECMDGOALS),clean)
-include $(DEPENDS)
endif
endif

