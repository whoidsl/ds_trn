########################################
# libtrnav/utils Makefile
# Build Environment
# version and build configuration

# uncomment and optionally define on command line
# to override value defined in source, e.g.
#   trn_ver=1.0.1 make
trn_ver ?=0.0.1
trn_build=`date +%Y/%m/%dT%H:%M:%S%z`
OPT_TRN_VER=-DLIBTRN_VERSION=$(trn_ver)
OPT_TRN_BUILD=-DLIBTRN_BUILD=$(trn_build)

# C standard e.g. -std=c99 -std=gnu99
# may be needed for Cygwin (e.g. for loop declare/init)
#STD= -std=c99

# Platform-specific options
ifeq ($(shell uname),Linux)
 OS_CFLAGS=
 OS_INC_PATH=
 OS_LIB_PATH=
 OS_LIBS=-lrt
endif

ifeq ($(shell uname),Darwin)
OS_CFLAGS=
OS_INC_PATH=-I/opt/local/include
OS_LIB_PATH=-L/opt/local/lib
OS_LIBS=
endif

ifeq ($(shell uname),Cygwin)
OS_CFLAGS=
OS_INC_PATH=-I/opt/local/include
OS_LIB_PATH=-L/opt/local/lib
OS_LIBS=-lrt
endif

# Build options
BUILD_OPTIONS = $(STD) -D_GNU_SOURCE  $(OPT_TRN_VER) $(OPT_TRN_BUILD)

# Build directories
OUTPUT_DIR=../bin
BUILD_DIR=../build
QNX_DIR=../qnx-utils
NEWMAT_DIR=../newmat
TRN_DIR=../terrain-nav
TRNW_DIR=../trnw

# Compilation Options
CXX = g++ #gcc
AR = ar
# compile and link with -pg for profiling support
# then do gprof <application> gmon.out for profile output to console
#GPROF= -pg
# use -Wall unless you know what you're doing
WARN_FLAGS=-Wall

SANI_FLAG =
#SANI_FLAG += -fsanitize=address

# Compiler flags
CFLAGS = -g -O2 $(SANI_FLAG) $(WARN_FLAGS) $(GPROF) $(BUILD_OPTIONS) $(OS_CFLAGS)
INC_PATHS =  -I.  -I$(NEWMAT_DIR) -I$(QNX_DIR) -I$(TRN_DIR) -I$(TRNW_DIR) $(OS_INC_PATH)

# Set to 1 to build lrtrn-server
# Requires LCM and lrauv-lcmtypes
BUILD_LRTRN=0

# Add includes for lrtrn-server if required
ifeq ($(BUILD_LRTRN),1)
LRAUVLCM_DIR=../../lrauv-lcmtypes/include
INC_PATHS += -I$(LRAUVLCM_DIR)
endif

# Linker flags
LD_FLAGS = -g $(GPROF)
LIB_PATHS =  -L$(OUTPUT_DIR) $(OS_LIB_PATH) -L/usr/local/lib

########################################
# Target Definitions

TRN_SERVER=trn-server
TRN_SERVER_SRC=trn_server.cpp
TRN_SERVER_OBJ = $(TRN_SERVER_SRC:%.cpp=$(BUILD_DIR)/%.o)
TRN_SERVER_LIBS =  -ltrn  -lnewmat -lqnx -lnetcdf -lm $(OS_LIBS)

BODUMP=bodump
BODUMP_SRC=bodump.cpp
BODUMP_OBJ = $(BODUMP_SRC:%.cpp=$(BUILD_DIR)/%.o)
BODUMP_LIBS = -ltrn  -lnewmat -lqnx -lnetcdf -lm -lpthread $(OS_LIBS)

LOGTEST=test-trn-log
LOGTEST_SRC=test_trn_log.cpp
LOGTEST_OBJ = $(LOGTEST_SRC:%.cpp=$(BUILD_DIR)/%.o)
LOGTEST_LIBS = -ltrn  -lnewmat -lqnx -lnetcdf -lm -lpthread $(OS_LIBS)

OCTREE2PATCHES=octree2patches
OCTREE2PATCHES_SRC=octree2Patches.cpp
OCTREE2PATCHES_OBJ = $(OCTREE2PATCHES_SRC:%.cpp=$(BUILD_DIR)/%.o)
OCTREE2PATCHES_LIBS = -ltrn  -lnewmat -lqnx -lnetcdf -lm -lpthread $(OS_LIBS)

GRDOCTREEMAKER=grd-octree-maker
GRDOCTREEMAKER_SRC=grdOctreeMaker.cpp
GRDOCTREEMAKER_OBJ = $(GRDOCTREEMAKER_SRC:%.cpp=$(BUILD_DIR)/%.o)
GRDOCTREEMAKER_LIBS = -ltrn  -lnewmat -lqnx -lnetcdf -lm -lpthread $(OS_LIBS)

CSVOCTREEMAKER=csv-octree-maker
CSVOCTREEMAKER_SRC=csvOctreeMaker.cpp
CSVOCTREEMAKER_OBJ = $(CSVOCTREEMAKER_SRC:%.cpp=$(BUILD_DIR)/%.o)
CSVOCTREEMAKER_LIBS = -ltrn  -lnewmat -lqnx -lnetcdf -lm -lpthread $(OS_LIBS)

TRANSOCT=transoct
TRANSOCT_SRC=translateOctree.cpp
TRANSOCT_OBJ = $(TRANSOCT_SRC:%.cpp=$(BUILD_DIR)/%.o)
TRANSOCT_LIBS = -ltrn  -lnewmat -lqnx -lnetcdf -lm -lpthread $(OS_LIBS)

TILETEST=tile-test
TILETEST_SRC=tile_test.cpp
TILETEST_OBJ = $(TILETEST_SRC:%.cpp=$(BUILD_DIR)/%.o)
TILETEST_LIBS = -ltrn  -lnewmat -lqnx -lnetcdf -lm -lpthread $(OS_LIBS)

TRNCLITEST=trnclient-test
TRNCLITEST_SRC= trnclient_test.cpp TrnClient.cpp TerrainNavClient.cpp
TRNCLITEST_OBJ = $(TRNCLITEST_SRC:%.cpp=$(BUILD_DIR)/%.o)
TRNCLITEST_LIBS = -ltrn  -lnewmat -lqnx -lnetcdf -lm -lpthread -lgeolib $(OS_LIBS)

LIBTRNCLI=libtrncli.a
LIBTRNCLI_SRC = TrnClient.cpp TerrainNavClient.cpp
LIBTRNCLI_OBJ = $(LIBTRNCLI_SRC:%.cpp=$(BUILD_DIR)/%.o)
LIBTRNCLI_LIBS = -ltrn  -lnewmat -lqnx -lnetcdf -lm -lpthread -lgeolib $(OS_LIBS)

TRNCLIUNIT=trnclient-unit
TRNCLIUNIT_SRC= trnclient_unit.cpp TrnClient.cpp TerrainNavClient.cpp
TRNCLIUNIT_OBJ = $(TRNCLIUNIT_SRC:%.cpp=$(BUILD_DIR)/%.o)
TRNCLIUNIT_LIBS = -ltrn  -lnewmat -lqnx -lnetcdf -lm -lpthread -lgeolib -lc $(OS_LIBS)

########################################
# Build Files (mostly for cleanup)
SOURCES = $(TRN_SERVER_SRC) $(BODUMP_SRC) \
		$(LOGTEST_SRC) $(OCTREE2PATCHES_SRC) \
		$(GRDOCTREEMAKER_SRC) $(CSVOCTREEMAKER_SRC) \
		$(TRANSOCT_SRC) $(TILETEST_SRC) \
		$(TRN_SERVER_SRC) $(TRNCLITEST_SRC) $(TRNCLIUNIT_SRC) \
		$(LIBTRNCLI_SRC)

OBJECTS = $(SOURCES:%.cpp=$(BUILD_DIR)/%.o)
DEPENDS = $(SOURCES:%.cpp=$(BUILD_DIR)/%.d)

BINARIES = $(OUTPUT_DIR)/$(LIBTRNCLI) \
		$(OUTPUT_DIR)/$(TRN_SERVER) \
		$(OUTPUT_DIR)/$(BODUMP) \
		$(OUTPUT_DIR)/$(LOGTEST) \
		$(OUTPUT_DIR)/$(OCTREE2PATCHES) \
		$(OUTPUT_DIR)/$(GRDOCTREEMAKER) \
		$(OUTPUT_DIR)/$(CSVOCTREEMAKER) \
		$(OUTPUT_DIR)/$(TRANSOCT) \
		$(OUTPUT_DIR)/$(TILETEST) \
		$(OUTPUT_DIR)/$(TRNCLITEST) \
		$(OUTPUT_DIR)/$(TRNCLIUNIT)

# Add lrtrn-server to sources and binaries if required
ifeq ($(BUILD_LRTRN),1)
SOURCES += $(OUTPUT_DIR)/$(LRTRN_SERVER_SRC)
BINARIES += $(OUTPUT_DIR)/$(LRTRN_SERVER)
endif


CLEANUP = gmon.out
# dSYMs : XCode debug symbol file folders
#DSYMS = $(BINARIES:%=%.dSYM)
#RM_DSYMS = rm -rf $(DSYMS)

########################################
# Rules: build targets

all: $(BINARIES)

$(OUTPUT_DIR)/$(LIBTRNCLI): $(LIBTRNCLI_OBJ)
	@echo building $@...
	ar -rcs $@ $(LIBTRNCLI_OBJ)
	ranlib $@

# build TRN client unit test
$(OUTPUT_DIR)/$(TRNCLIUNIT): $(TRNCLIUNIT_OBJ)
	@echo building $@...
	$(CXX) $(CFLAGS) $(INC_PATHS) $(LIB_PATHS) $^ -o $@ $(LD_FLAGS) $(TRNCLIUNIT_LIBS)
	@echo

# build TRN server utility
$(OUTPUT_DIR)/$(TRN_SERVER): $(TRN_SERVER_OBJ)
	@echo building $@...
	$(CXX) $(CFLAGS) $(INC_PATHS) $(LIB_PATHS) $^ -o $@ $(LD_FLAGS) $(TRN_SERVER_LIBS)
	@echo

# build binary octree dump utility
$(OUTPUT_DIR)/$(BODUMP): $(BODUMP_OBJ)
	@echo building $@...
	$(CXX) $(CFLAGS) $(INC_PATHS) $(LIB_PATHS) $^ -o $@ $(LD_FLAGS) $(BODUMP_LIBS)
	@echo

# build trn log test utility
$(OUTPUT_DIR)/$(LOGTEST): $(LOGTEST_OBJ)
	@echo building $@...
	$(CXX) $(CFLAGS) $(INC_PATHS) $(LIB_PATHS) $^ -o $@ $(LD_FLAGS) $(LOGTEST_LIBS)
	@echo

# build octree2patches utility
$(OUTPUT_DIR)/$(OCTREE2PATCHES): $(OCTREE2PATCHES_OBJ)
	@echo building $@...
	$(CXX) $(CFLAGS) $(INC_PATHS) $(LIB_PATHS) $^ -o $@ $(LD_FLAGS) $(OCTREE2PATCHES_LIBS)
	@echo

# build grid octree maker utility
$(OUTPUT_DIR)/$(GRDOCTREEMAKER): $(GRDOCTREEMAKER_OBJ)
	@echo building $@...
	$(CXX) $(CFLAGS) $(INC_PATHS) $(LIB_PATHS) $^ -o $@ $(LD_FLAGS) $(GRDOCTREEMAKER_LIBS)
	@echo

# build CSV octree maker utility
$(OUTPUT_DIR)/$(CSVOCTREEMAKER): $(CSVOCTREEMAKER_OBJ)
	@echo building $@...
	$(CXX) $(CFLAGS) $(INC_PATHS) $(LIB_PATHS) $^ -o $@ $(LD_FLAGS) $(CSVOCTREEMAKER_LIBS)
	@echo

# build translate octree utility
$(OUTPUT_DIR)/$(TRANSOCT): $(TRANSOCT_OBJ)
	@echo building $@...
	$(CXX) $(CFLAGS) $(INC_PATHS) $(LIB_PATHS) $^ -o $@ $(LD_FLAGS) $(TRANSOCT_LIBS)
	@echo

# build tile test utility
$(OUTPUT_DIR)/$(TILETEST): $(TILETEST_OBJ)
	@echo building $@...
	$(CXX) $(CFLAGS) $(INC_PATHS) $(LIB_PATHS) $^ -o $@ $(LD_FLAGS) $(TILETEST_LIBS)
	@echo

# build trncli-test utility
$(OUTPUT_DIR)/$(TRNCLITEST): $(TRNCLITEST_OBJ)
	@echo building $@...
	$(CXX) $(CFLAGS) $(INC_PATHS) $(LIB_PATHS) $^ -o $@ $(LD_FLAGS) $(TRNCLITEST_LIBS)

# generate dependency files
ifneq ($(MAKECMDGOALS),clean)
ifneq ($(MAKECMDGOALS),purge)
-include $(DEPENDS)
endif
endif

# rule: build object files from source files
$(BUILD_DIR)/%.o :%.cpp
	@echo compiling $<...
	$(CXX) $(CFLAGS) $(INC_PATHS) -c $< -o $@
	@echo

# rule: build dependency files from source files
$(BUILD_DIR)/%.d :%.cpp
	@[ -d $(BUILD_DIR) ] || mkdir -p $(BUILD_DIR)
	@[ -d $(OUTPUT_DIR) ] || mkdir -p $(OUTPUT_DIR)
	@echo generating dependency file for $<
	@set -e; $(CXX) -MM $(CFLAGS) $(INC_PATHS) $< \
	| awk '/o:/ {printf "%s", "$@ $(BUILD_DIR)/"} {print}' > $@; \
	[ -s $@ ] || rm -f $@
	@echo

install:
	@echo "make install...(not implemented)"


########################################
# Rules:

.PHONY: clean
.PHONY: purge

# clean : delete object, dependency, binary files
clean:
	rm -f $(OBJECTS) $(DEPENDS) $(BINARIES)
	$(RM_DSYMS)

# purge : delete delete object, dependency, binary files, build directories
purge:
	rm -f $(BINARIES) $(OBJECTS) $(DEPENDS) $(CLEANUP)
	rm -rf $(OUTPUT_DIR) $(BUILD_DIR) $(DSYMS)

