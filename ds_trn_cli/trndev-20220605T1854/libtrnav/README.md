# libtrnav

Core terrain relative navigation libraries and utilities, developed through a collaboration between MBARI and Stanford. 
Previously hosted in the MBARI auv-qnx CVS repo in the G2TerrainNav directory.

## Developers 
Important: 
 * *Do not change anything in the FastSim, master_orig or reorg branches*, which preserve the original migration state from CVS.
 * When naming files, take care to use file names that will not conflict in case-insensitive filesystems (e.g. under Cygwin)


## Platforms
Builds on Linux, OS X, Cygwin

## Dependencies

	libnetcdf
	libpthread
	libm
	
## Contents

### Libraries

	libtrn			Core TRN API
    	libnewmat		New math library
    	libgeolib		libgctp-2.0 geocoordinate transformation
    	libqnx-utils	Utilities
    	
### Applications
TRN applications

	trn-server
	trn-replay
	bodump
	octree2patches
	grd-octree-maker
	csv-octree-maker
	transoct
	tile-test
	test-trn-log
	mbari-main

QNX Utils

	readlog
	writelog
	log-to-matlab
	log-to-csv
	
## Building
To build all libraries and applications use  
	
	make 
Use

	make help

to see other make targets
CAUTION: *Do not change anything in the FastSim, master_orig or reorg branches*, which preserve the original migration state from CVS.
