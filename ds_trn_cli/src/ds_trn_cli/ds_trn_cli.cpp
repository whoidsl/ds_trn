/**
 * Copyright 2018 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "ds_trn_cli/ds_trn_cli.h"
#include <fstream>
#include <ios>
#include <iostream>
#include <regex>
#include <stdio.h>

namespace ds_trn {
// Define the self_handle
TrnCli *TrnCli::self_handle = NULL;

TrnCli::TrnCli(bool start_client) : ds_base::DsProcess(), active_timer(false) {
  last_user_shift_time = ros::Time::now();
  last_user_shift_easting = 0;
  last_user_shift_northing = 0;
  shift_mult_factor_ = 1;
  swap_trn_xy_ = 0;
  enable_forwarding_ = 0;
  max_shift_difference = 200;
  swap_point_calc_sign = 0;
 
  self_handle = this;
  if (start_client) {
    initializeClient();
    startClient();
  }
}

TrnCli::TrnCli(int argc, char *argv[], const std::string &name)
    : DsProcess(argc, argv, name), active_timer(false) {
  last_user_shift_time = ros::Time::now();
  last_user_shift_easting = 0;
  last_user_shift_northing = 0;
  shift_mult_factor_ = 1;
  swap_trn_xy_ = 0;
  enable_forwarding_ = 0;
  max_shift_difference = 200;
  swap_point_calc_sign = 0;

  self_handle = this;
  initializeClient();
  startClient();
}

TrnCli::~TrnCli() {
  // Not called when ctrl-c ends the rosnode since the SIGINT made by ctrl-c
  // causes immediate/abnormal program exit
  shutdownClient();
}

void TrnCli::setupConnections() {
  ds_base::DsProcess::setupConnections();

  ext_conn_ = addConnection("ext_connection",
                            boost::bind(&TrnCli::onExtData, this, _1));
}

void TrnCli::setupSubscriptions() {
  ds_base::DsProcess::setupSubscriptions();

  // MC - Subscribe to the JTROS message that includes MCA SHFTABS sent
  // acoustically - store last user shift
  std::string jtros_topic;
  ros::param::param<std::string>("~jtros_topic", jtros_topic, "/mca/jtros");
  ROS_INFO_STREAM("Jtros topic: " << jtros_topic);
  mca_shftabs_sub_ = nodeHandle().subscribe<std_msgs::String>(
      jtros_topic, 1, boost::bind(&TrnCli::onMcaJtrosMsg, this, _1));
  // MX - TODO
}

void TrnCli::setupPublishers() {
  ds_base::DsProcess::setupPublishers();

  std::string topic;
  ros::param::param<std::string>("~trn_update_topic", topic,
                                 "/ds_trn_cli/TrnUpdate");
  ROS_INFO_STREAM("topic is " << topic);
  m_pub = nodeHandle().advertise<ds_trn_cli::TrnUpdate>(topic, 10);

  // MC - create publisher that could send JTROS message with MCA SHFTABS to
  // mc-shim
  std::string topic_mc;
  ros::param::param<std::string>("~jtros_bd_topic", topic_mc,
                                 "/sentry/mission/backdoor");
  ROS_INFO_STREAM("topic mc backdoor publisher is " << topic_mc);
  mca_shftabs_pub_ = nodeHandle().advertise<std_msgs::String>(topic_mc, 10);
}

void TrnCli::setupParameters() {
  ds_base::DsProcess::setupParameters();
  auto nh = nodeHandle();
  if (nh.getParam(ros::this_node::getName() +
                      "/shift_constants/max_shift_difference",
                  max_shift_difference)) {
    ROS_WARN_STREAM("Initialized max shift difference magnitude from yaml to "
                    << max_shift_difference);
  } else {
    ROS_ERROR("Failed to read 'Maximum shift difference magnitude' on "
              "param server");
  }
  if (nh.getParam(ros::this_node::getName() +
                      "/shift_constants/swap_point_calc_sign",
                  swap_point_calc_sign)) {
    ROS_WARN_STREAM("Initialized swap point calculation sign to "
                    << swap_point_calc_sign);
  } else {
    ROS_ERROR("Failed to read 'Maximum shift difference magnitude' on "
              "param server");
  }
}

void TrnCli::setupTimers() {
  ds_base::DsProcess::setupTimers();

  timeout_timer =
      nodeHandle().createTimer(ros::Duration(ros::Duration(TIMEOUT_DECREMENT)),
                               &TrnCli::timeout_updater, this);
}

void TrnCli::setupServices() {
  ds_base::DsProcess::setupServices();

  auto nh = nodeHandle();

  // this binds any service request to the send command callback
  reset_service = nh.advertiseService<ds_trn_cli::ResetTRNCmd::Request,
                                      ds_trn_cli::ResetTRNCmd::Response>(
      ros::this_node::getName() + "/reset_TRN",
      boost::bind(&TrnCli::reset_TRN, this, _1, _2));
  reset_offset_service =
      nh.advertiseService<ds_trn_cli::ResetTRNOffsetCmd::Request,
                          ds_trn_cli::ResetTRNOffsetCmd::Response>(
          ros::this_node::getName() + "/reset_TRN_Offset",
          boost::bind(&TrnCli::reset_TRN_ofs_trn, this, _1, _2));
  con_discon_service = nh.advertiseService<ds_trn_cli::ConDisconCmd::Request,
                                           ds_trn_cli::ConDisconCmd::Response>(
      ros::this_node::getName() + "/con_discon_TRN",
      boost::bind(&TrnCli::con_discon_TRN, this, _1, _2));
  shift_forwarding_service =
      nh.advertiseService<ds_trn_cli::ShiftForwardingCmd::Request,
                          ds_trn_cli::ShiftForwardingCmd::Response>(
          ros::this_node::getName() + "/shift_forwarding_TRN",
          boost::bind(&TrnCli::control_shift_forwarding, this, _1, _2));
}

void TrnCli::onExtData(ds_core_msgs::RawData bytes) {
  // Parse external commands sent from another process via the connection
  // Typically used to interface to this via a general purpose UDP acomms
  // queue

  std::string extData(bytes.data.begin(), bytes.data.end());

  ROS_INFO_STREAM("Received external data: " << extData);

  // TODO - TRN ENABLE xxxxx yyy z (where xxxxx is the timeout in seconds, yyy
  // is a multiplicative factor of the shift, and z is a boolean that
  // indicates to swap shift x and y)
  if (!extData.find("TRN ENABLE")) {
    int status = 0;
    int timeout = 0;
    status = sscanf(extData.c_str(), "TRN ENABLE %d %d %d", &timeout,
                    &shift_mult_factor_, &swap_trn_xy_);
    if (status == 3) {
      ds_trn_cli::ShiftForwardingCmd::Request req;
      req.command = req.ENABLE;
      req.timeout = ros::Duration(timeout);
      ds_trn_cli::ShiftForwardingCmd::Response resp;
      bool success = control_shift_forwarding(req, resp);
    }
  }
  // TRN DISABLE
  else if (!extData.find("TRN DISABLE")) {
    ds_trn_cli::ShiftForwardingCmd::Request req;
    req.command = req.DISABLE;
    ds_trn_cli::ShiftForwardingCmd::Response resp;
    bool success = control_shift_forwarding(req, resp);
  }

  // TRN RESET (resets the trn particle filter)
  // corresponds with reinit of mbtrnpp
  else if (!extData.find("TRN RESET")) {

    bool success;

    int status = 0;
    double ofs_x;
    double ofs_y;
    double ofs_z;
    int swap_signs;
    status = sscanf(extData.c_str(), "TRN RESET %lf %lf %lf %d", &ofs_x, &ofs_y,
                    &ofs_z, &swap_signs);
    if (status == 3 || status == 4) {
      ds_trn_cli::ResetTRNOffsetCmd::Request req;
      ds_trn_cli::ResetTRNOffsetCmd::Response resp;
      req.ofs_x = ofs_x;
      req.ofs_y = ofs_y;
      req.ofs_z = ofs_z;
      if (status == 3) {
        // don't swap the signs. acomms message didnt contain that val
        req.swap_signs = 0;
      } else {
        req.swap_signs = swap_signs;
      }
      ROS_WARN_STREAM("Resetting particle filter to requested offset of "
                      << ofs_x << " " << ofs_y << " " << ofs_z);
      success = reset_TRN_ofs_trn(req, resp);

    } else {
      ds_trn_cli::ResetTRNCmd::Request req;
      ds_trn_cli::ResetTRNCmd::Response resp;
      ROS_WARN_STREAM("Resetting particle filter to default");
      success = reset_TRN(req, resp);
    }
  }

  // TRN RESTORE (restores the last good user shift)
  else if (!extData.find("TRN RESTORE")) {
    // HERE restore the previous shift that sentry operators sent acoustically
    std::ostringstream mca{};
    mca << "MCA SHFTABS " << last_user_shift_easting << " "
        << last_user_shift_northing;
    ROS_INFO_STREAM("Restoring user shift after command: " << mca.str());
    std_msgs::String out;
    out.data = mca.str();
    mca_shftabs_pub_.publish(out);
  }

  // enable forwarding
  else if (!extData.find("TRN FORWARD_ENABLE")) {
    ROS_WARN_STREAM("Enabling shift forwarding");
    enable_forwarding_ = 1;
  }

  // disable forwarding
  else if (!extData.find("TRN FORWARD_DISABLE")) {
    ROS_WARN_STREAM("Disabling shift forwarding");
    enable_forwarding_ = 0;
  }

  else
    ROS_WARN_STREAM("Unrecognized command");
}

void TrnCli::onMcaJtrosMsg(const std_msgs::String::ConstPtr msg) {
  std::string data = msg->data;
  std::string out;

  ROS_WARN_STREAM("Received: " << data);

  const auto equals_idx = data.find_first_of(' ');
  if (std::string::npos != equals_idx) {
    out = data.substr(equals_idx + 1);
    ROS_ERROR_STREAM(out);
  } else {
    ROS_ERROR_STREAM(out);
  }

  if (!out.find("SHFTABS")) {
    double shift_easting = 0;
    double shift_northing = 0;
    int status = 0;
    status =
        sscanf(out.c_str(), "SHFTABS %lf %lf", &shift_easting, &shift_northing);
    ROS_WARN_STREAM("Parsing: " << out);
    if (status == 2) {
      last_user_shift_time = ros::Time::now();
      last_user_shift_easting = shift_easting;
      last_user_shift_northing = shift_northing;

      ROS_WARN_STREAM("Updated user shift: " << last_user_shift_easting << " "
                                             << last_user_shift_northing);
    }
  }
}

void TrnCli::timeout_updater(const ros::TimerEvent &) {
  // decrement the current timer value by the decrement timestop value
  // (TIMEOUT_DECREMENT)
  if (self_handle->active_timer) {
    // if we reach the end of the timeout, make sure we set the timer as
    // inactive.
    if (self_handle->shift_time <= 0) {
      self_handle->active_timer = false;
      return;
    }
    self_handle->shift_time -= TIMEOUT_DECREMENT;
  } else {
    // HERE restore the previous shift that sentry operators sent acoustically
    std::ostringstream mca{};
    mca << "MCA SHFTABS " << last_user_shift_easting << " "
        << last_user_shift_northing;
    ROS_INFO_STREAM("Restoring user shift in timer: " << mca.str());
    std_msgs::String out;
    out.data = mca.str();
    cmd_string = mca.str();
    if (enable_forwarding_ == 1) {
      mca_shftabs_pub_.publish(out);
    }
  }
}

void TrnCli::initializeClient() {
  cfg = (app_cfg_t *)malloc(sizeof(app_cfg_t));
  ctx = (trnucli_ctx_t *)malloc(sizeof(trnucli_ctx_t));

  std::string host;
  auto nh = nodeHandle();

  if (nh.getParam(ros::this_node::getName() + "/host", host)) {
    if (NULL != cfg) {
      memset(cfg, 0, sizeof(app_cfg_t));
      cfg->verbose = false;
      cfg->ifile = NULL;
      cfg->input_src = TRNUCLI_TEST_SRC;
      cfg->trnu_host = (char *)host.c_str();
      cfg->trnu_port = TRNUCLI_TEST_TRNU_PORT;
      cfg->ofmt = TRNUCLI_TEST_OFMT;
      cfg->trnu_hbeat = TRNUCLI_TEST_TRNU_HBEAT;
      // cfg->hbeat_to_sec=TRNUCLI_TEST_HBEAT_TMOUT_SEC;
      cfg->hbeat_to_sec = 10;
      cfg->update_n = TRNUCLI_TEST_UPDATE_N;
      cfg->log_cfg =
          mlog_config_new(ML_TFMT_ISO1806, ML_DFL_DEL,
                          mlog_flags_t(ML_MONO | ML_NOLIMIT), ML_FILE, 0, 0, 0);
      cfg->log_id = MLOG_ID_INVALID;
      cfg->log_name = strdup(TRNUCLI_TEST_LOG_NAME);
      cfg->log_dir = strdup(TRNUCLI_TEST_LOG_DIR);
      cfg->log_path = (char *)malloc(512);
      cfg->recon_to_sec = TRNUCLI_TEST_RECON_TMOUT_SEC;
      cfg->recon_timer = 0.0;
      cfg->listen_to_ms = TRNUCLI_TEST_LISTEN_TMOUT_MSEC;
      cfg->enodata_delay_ms = TRNUCLI_TEST_ENODATA_DELAY_MSEC;
      cfg->erecon_delay_ms = TRNUCLI_TEST_ERECON_DELAY_MSEC;
      cfg->stats_log_period_sec = TRNUCLI_TEST_STATS_LOG_PERIOD_SEC;
      cfg->ctx_flags = TRNUCLI_TEST_LOG_EN;
      cfg->ofile = TRNUCLI_TEST_OFILE;
    }

    // configure and return a trnu async context instance
    // caller must release resources using trnucli_ctx_destroy
    // host             : host/multicast group
    // port             : port
    // trnu_ttl              : multicast time to live (TTL)
    // ctx_flags        : context option flags (logging, multicast)
    // cli_flags        : client option flags (blocking, multicast)
    // hbeat_to_sec     : heartbeat timeout (s); set <=0 to disable
    // recon_to_sec     : reconnect timeout (if no data available for
    // recon_to_sec) listen_to_ms     : listen timeout (socket SO_RCVTIMEO for
    // blocking reads) enodata_delay_ms : delay if no data available (msec)
    // erecon_delay_ms  : delay between reconnect attempts (msec)
    // update_fn        : optional update callback (NULL to disable)
    // returns new instance
    update_callback_fn ucf = trnu_update_callback;
    ctx = trnucli_ctx_new(
        cfg->trnu_host, cfg->trnu_port,
        cfg->trnu_ttl,  // int, multicast ttl (are we doing multicast?)
        cfg->ctx_flags, // "ctx_flags", context option flags (logging,
                        // multicast) cfg->flags = CTX_LOG_EN
        cfg->cli_flags, // "cli_flags", client option flags (blocking,
                        // multicast).
        cfg->hbeat_to_sec, cfg->recon_to_sec, cfg->listen_to_ms,
        cfg->enodata_delay_ms, cfg->erecon_delay_ms, ucf);
  } else {
    ROS_ERROR_STREAM(
        "ERR - TRN Client start failure- IP address not provided\n");
  }
}

void TrnCli::startClient() {
  if (NULL != ctx) {
    // configure stats logging
    trnucli_ctx_set_stats_log_period(ctx, cfg->stats_log_period_sec);

    // start the client - separate worker thread
    //  - manages connection (reconnects on timeout)
    //  - receives updates w/ optional update handler callback
    int test = trnucli_ctx_start(ctx);

    if (test == 0) {
      // success
      ROS_INFO_STREAM("ctx start OK\n");
    } else {
      ROS_ERROR_STREAM("ERR - ctx start failed\n");
      mlog_tprintf(cfg->log_id, "ERR - ctx start failed\n");
    }
  }
}

void TrnCli::shutdownClient() {
  if (NULL != ctx) {
    ROS_WARN_STREAM("user interrupt - stopping\n");
    trnucli_ctx_stop(ctx);
    mlog_tprintf(cfg->log_id, "user interrupt - stopping\n");

    // release client resources
    ROS_INFO_STREAM("destroying ctx\n");
    mlog_tprintf(cfg->log_id, "destroying ctx\n");
    trnucli_ctx_destroy(&ctx);
  }
}

void TrnCli::publishTRNUpdate(trnu_pub_t *update) {
  ds_trn_cli::TrnUpdate msg;

  msg.header.stamp = ros::Time::now();

  msg.sync = update->sync;
  msg.update_time = update->update_time;
  msg.success = update->success;
  msg.reinit_tlast = update->reinit_tlast;
  msg.reinit_count = update->reinit_count;
  msg.ping_number = update->ping_number;
  msg.mb1_time = update->mb1_time;
  msg.mb1_cycle = update->mb1_cycle;
  msg.is_valid = update->is_valid;
  msg.is_converged = update->is_converged;
  msg.filter_state = update->filter_state;
  memcpy(&(msg.est[0]), &(update->est[0]), sizeof(ds_trn_cli::TrnEstimate));
  memcpy(&(msg.est[1]), &(update->est[1]), sizeof(ds_trn_cli::TrnEstimate));
  memcpy(&(msg.est[2]), &(update->est[2]), sizeof(ds_trn_cli::TrnEstimate));
  memcpy(&(msg.est[3]), &(update->est[3]), sizeof(ds_trn_cli::TrnEstimate));
  memcpy(&(msg.est[4]), &(update->est[4]), sizeof(ds_trn_cli::TrnEstimate));

  // BEGIN ERROR ELLIPSE CALCULATIONS //

  // find the semi-major axis, semi-minor axis, and direction of the error
  // ellipse for the realtime nav data from TRN server See
  // https://cookierobotics.com/007/ for details on the mathematics

  for (int i = 0; i < 5; i++) {
    double a = update->est[i].cov[0]; // the variance of x for pose
    double c = update->est[i].cov[1]; // the variance of y for pose
    double b =
        update->est[i].cov[3]; // the covariance of xy (which is equal to yx)
    double sqrt_term = sqrt(((a - c) / 2.0) * ((a - c) / 2.0) + b * b);
    double fraction_term = (a + c) / 2.0;
    msg.error_ellipse_semimajor_axis_lengths[i] =
        sqrt(fraction_term + sqrt_term);
    msg.error_ellipse_semiminor_axis_lengths[i] =
        sqrt(fraction_term - sqrt_term);

    // now, the ellipse's direction
    if (0 == b && a >= c)
      msg.error_ellipse_directions[i] = 0;
    if (0 == b && a < c)
      msg.error_ellipse_directions[i] = M_PI / 2.0;
    else
      msg.error_ellipse_directions[i] = atan2(fraction_term + sqrt_term - a, b);
  }

  //get magnitude of covar
  double magnitude = sqrt(update->est[2].cov[0] + update->est[2].cov[1] + update->est[2].cov[2]);
  msg.magnitude = magnitude;

  // END ERROR ELLIPSE CALCULATIONS //

  // FIND THE TRN SHIFT
  msg.trn_shift.delta_x = update->est[1].x - update->est[0].x;
  msg.trn_shift.delta_y = update->est[1].y - update->est[0].y;
  msg.trn_shift.delta_z = update->est[1].z - update->est[0].z;

  // Find the difference of shift between TRN shift and the mission shift, but
  // ONLY FOR CERTAIN PERIODS OF TIME
  if (active_timer) {
    // These mission shift calculations are placeholders. After the
    // engineering dives we'll figure out how the reference frames will work
    // for computing the right shift here
    msg.trn_mission_shift.delta_x = 0.0 - msg.trn_shift.delta_x;
    msg.trn_mission_shift.delta_y = 0.0 - msg.trn_shift.delta_y;
    msg.trn_mission_shift.delta_z = 0.0 - msg.trn_shift.delta_z;
  } else {
    msg.trn_mission_shift.delta_x = -1;
    msg.trn_mission_shift.delta_y = -1;
    msg.trn_mission_shift.delta_z = -1;
  }

  m_pub.publish(msg);

  if (self_handle->active_timer) {
    // HERE forward last good TRN shift (if it exists) to MC or MX if the
    // timer is active send on mca_shftabs_pub as jtros message
    int shift_easting = 0;
    int shift_northing = 0;
    // Normally SHFTABS -est[4].y -est[4].x
    if (swap_trn_xy_ == 1) {
      shift_easting = -1 * static_cast<int>(msg.est[4].x);
      shift_northing = -1 * static_cast<int>(msg.est[4].y);
    } else {
      shift_easting = -1 * static_cast<int>(msg.est[4].y);
      shift_northing = -1 * static_cast<int>(msg.est[4].x);
    }
    shift_easting = shift_easting * shift_mult_factor_;
    shift_northing = shift_northing * shift_mult_factor_;

    std::ostringstream mca{};

    mca << "MCA SHFTABS " << shift_easting << " " << shift_northing;
    ROS_INFO_STREAM("Sending last valid TRN shift: " << mca.str());
    std_msgs::String out;
    out.data = mca.str();
    cmd_string = mca.str();
    
    if (enable_forwarding_ == 1) {
      double calc_dist = calculatePointDistance(
          double(last_user_shift_easting), double(last_user_shift_northing),
          double(shift_easting), double(shift_northing));
      ROS_INFO_STREAM("Last user shift easting: " << last_user_shift_easting);
      ROS_INFO_STREAM("Last user shift northing: " << last_user_shift_northing);
      ROS_INFO_STREAM("Calculated shift easting: " << shift_easting);
      ROS_INFO_STREAM("Calculated shift northing: " << shift_northing);
      ROS_INFO_STREAM("Calculted point distance: " << calc_dist);

      if (calc_dist < max_shift_difference) {
        ROS_INFO_STREAM(
            "OK! Forwarding shift to MC. Calculated shift difference: "
            << calc_dist << " is under threshold ");
        mca_shftabs_pub_.publish(out);
      } else {
        ROS_WARN_STREAM(
            "Not forwarding shift to MC. Calculated shift difference: "
            << calc_dist << " greater than threshold");
      }
    }
  }

  // Populate acomms queue and send via ext_conn_
  // USER_X USER_Y ENGAGED REMAINING_TIMEOUT TRN_SHFT_X TRN_SHFT_Y IS_VALID

  //shorten the sdq queue without affect any other part of the code that uses mca or cmd_string;
  std::string cmd_copy = cmd_string;
  std::string prefix = "MCA SHFTABS ";

  if (cmd_copy.compare(0, prefix.length(), prefix) == 0) {
    cmd_copy.erase(0, prefix.length());
  }

  std::ostringstream out{};
  out << last_user_shift_easting << " " << last_user_shift_northing << " "
      << enable_forwarding_ << " "
      << static_cast<int>(self_handle->active_timer) << " "
      << self_handle->shift_time << " " << static_cast<int>(msg.est[3].x) << " "
      << static_cast<int>(msg.est[3].y) << " " << static_cast<int>(msg.is_valid)
      << " " << static_cast<int>(msg.est[4].x) << " "
      << static_cast<int>(msg.est[4].y) << " " << cmd_copy << " " << magnitude;

  ext_conn_->send(out.str());
}

bool TrnCli::reset_TRN(ds_trn_cli::ResetTRNCmd::Request &req,
                       ds_trn_cli::ResetTRNCmd::Response &resp) {
  // return: 0 on success, -1 otherwise
  ROS_INFO_STREAM("Calling reset TRN api");
  int is_reset = trnucli_ctx_reset_trn(ctx);
  bool r = is_reset == 0 ? true : false;
  resp.result = r;
  return r;
}

bool TrnCli::reset_TRN_ofs_trn(ds_trn_cli::ResetTRNOffsetCmd::Request &req,
                               ds_trn_cli::ResetTRNOffsetCmd::Response &resp) {
  // return: 0 on success, -1 otherwise

  double ofs_x = req.ofs_x;
  double ofs_y = req.ofs_y;
  double ofs_z = req.ofs_z;
  int swap = req.swap_signs;
  if (swap == 1) {
    ROS_INFO_STREAM("Flipping signs of reset values");
    ofs_x = -ofs_x;
    ofs_y = -ofs_y;
    ofs_z = -ofs_z;
  }
  ROS_WARN_STREAM("Calling reset TRN offset API with values "
                  << ofs_x << " " << ofs_y << " " << ofs_z);
  int is_reset = trnucli_ctx_reset_ofs_trn(ctx, ofs_x, ofs_y, ofs_z);
  bool r = is_reset == 0 ? true : false;
  resp.result = r;
  return r;
}

bool TrnCli::con_discon_TRN(ds_trn_cli::ConDisconCmd::Request &req,
                            ds_trn_cli::ConDisconCmd::Response &resp) {
  ROS_INFO_STREAM("PATH 0\n");
  // see if we're connected
  // return: non-zero (-1 specifically) if connected, 0 otherwise
  int is_connected = trnucli_ctx_isconnected(ctx);
  int32_t con_result = -1;

  // if already connected and wanting to reconnect
  if (is_connected != 0 && req.CONNECT_TO_TRN == req.command) {
    con_result = req.OP_SUCCESS;
    // do nothing, possibly reconnect?
  }

  // if already connected && wanting to disconnect
  if (is_connected != 0 && req.DISCONNECT_FROM_TRN == req.command) {
    // let's disconnect
    // stop async trncli thread (disconnects from data host)
    // return: 0 on success, -1 otherwise
    con_result = trnucli_ctx_stop(ctx);
  }

  // if disconnected and wanting to connect
  if (is_connected == 0 && req.CONNECT_TO_TRN == req.command) {
    // let's connect
    // start async trncli thread
    // return: 0 on success, -1 otherwise
    con_result = trnucli_ctx_start(ctx); // check connection status
  }

  // if disconnected and wanting to disconnect
  if (is_connected == 0 && req.DISCONNECT_FROM_TRN == req.command) {
    // nothing to do here, either
    con_result = req.OP_SUCCESS;
  }

  resp.result = con_result == 0 ? req.OP_SUCCESS : req.OP_FAILURE;

  // These always have to return true otherwise the rosservice call will throw
  // an error.
  return true;
}

bool TrnCli::control_shift_forwarding(
    ds_trn_cli::ShiftForwardingCmd::Request &req,
    ds_trn_cli::ShiftForwardingCmd::Response &resp) {
  // A service that, given an enable/disable flag and a timeout parameter,
  // disable the shift forwarding to nav or enables (with timeout in seconds)
  // the shift forwarding to nav (where the shift is the difference of the
  // mission shift and the TRN shift)
  if (req.ENABLE == req.command) {
    // enable forwarding
    // start the timer
    timeout_timer.start(); // Note: even if a timer is already running,
                           // calling this command will do no harm.
    active_timer = true;
    shift_time = req.timeout.sec; // set or override the timer with the
                                  // requested timeout value
  }
  if (req.DISABLE == req.command && !active_timer) {
    // if we want to disable and there's no active timer, do nothing
  }
  if (req.DISABLE == req.command && active_timer) {
    // if we want to disable the timer and there is an active timer
    active_timer = false;
    shift_time = 0;
    // timeout_timer.stop();
  }
  std::cout << "test" << std::endl;
  resp.result = req.OP_SUCCESS;

  return true;
}

double TrnCli::calculatePointDistance(double x1, double y1, double x2,
                                      double y2) {
  if (swap_point_calc_sign >= 1){
    x2 = -x2;
    y2 = -y2;
  }
  return sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2));
}

int TrnCli::getSwapCalcPointSign(){
  return swap_point_calc_sign;
}
}; // namespace ds_trn
