# How to build the ds_trn_cli_node TRN client node:

## Building

    cd into trndev-gnumake and follow build instructions (below)
    cd mframe
    make

    cd ../libtrnav
    make all trnc
    cd ../
    sudo cp include/libtrnav-0.1.0/* /usr/local/include/libtrnav-0.1.0/
    sudo cp include/mframe-0.1.0/* /usr/local/include/mframe-0.1.0/
    sudo cp libtrnav/bin/*.a /usr/local/lib/
    sudo cp mframe/bin/libmframe.a /usr/local/lib
    cd ~/sentry_ws/
    catkin_make
    catkin_make run_tests_ds_trn_cli

** This build does manually copy some headers and libs into your path. Take care 
to remove them if uninstalling. Kent Headly from MBARI could likely provide a 
install/uninstall script, but that was not used in 2022-nooner testing due not introducing 
changes mid dives.
    

There are some dependencies listed in ds_trn_cli/scripts in a sh script.
I do not recommend that be run on a vehicle directly unless needed. May update
many packages.
    
There exists a cmake build as well in the trn-dev2022* folder. It is untested.
The current build uses gnu make. The build script accepts a CMake binary as a parameter.
Usage is displayed if you do not enter the param.
    

# How to launch ds_trn_cli_node (don't forget to source ~/sentry_ws/devel/setup.bash before running this)

It is recommended to run this before the server TRN software. Restarting the node is also recommended if restarting ROS, due to different masters.    
roslaunch ds_trn_cli ds_trn_cli.launch    

** Tip: The stdoutput and debug output of this node can be very usefulfor debugging, 
consider redirecting the output of this roslaunch to a text file if needed....possibly with tee.     



## Changable ROS Node PArameters:

max_shift_difference:
The threshold othe max allowed difference  of 2d point distance between the last mc user shift, and trn calculated shift (in meters). If the difference is higher than this threshold, will not forward shift to mc.     
swap_point_calc_sign: whether to swap the trn shift signs during point distance calc. LEave this on 0, as working with mbari that way.    
host: the trn server ip. Likely the datapod!    


## Kongsberg Setup

The TRN software on the datapod needs to receive kmalls from udp messages broadcasted. This is currently configured on sentry via a multicast udp broadcast.
Verify the datapod is configured to listen to the correct ip and port.    

You should be able to see the kongsberg broadcast ports on its kctrl docker web interface.multicast addresses usually start with 239. or a value close to that
Sentry is configured to recreate and destroy its kctrl docker interface on startup/stop. To make settings persist, you may have to edit the actual docker file with docker load.
Docker kctrl address is up on web browser. 127.0.0.1:8088. There is some further info on the sentry kongsberg wiki page.     


## Acoustic commands. Tested on sentry through sms and umodem

* Most of these commands have equivelant functionality through ros service calls exposed by this node     
* The commands go through a UDP general acomms queue. You can preface commands with UDP0 for acomms. Other queues may work, but were not tested. Use UPD0    

Examples through acomms:    

UDP0 TRN ENABLE 900 1 0     
UDP0 TRN DISABLE      
UDP0 TRN FORWARD_DISABLE      
UDP0 TRN FORWARD_ENABLE      
UDP0 TRN ENABLE 90000 1 0     
UDP0 TRN RESET     
UDP0 TRN RESET 300 200 0 1     
UDP0 TRN RESET 300 200 0     
UDP0 TRN RESTORE      

node's acoustic command set:     
◦ TRN ENABLE xxxxx yyy z (One of two commands needed to enable TRN shift-
forwarding to MC, where xxxxx is the timeout in seconds, yyy is a
multiplicative factor of the shift, and z is a boolean that indicates to swap
shift x and y. TRN FORWARD_ENABLE should be used in combination with it.)     
◦ TRN DISABLE (Sets the TRN mission shift to -1x,-1y,-1z in TRNUpdate
messages and prevents the node from sending TRN shifts to the MC-shim; it
doesn't prevent the node from restoring MC's last acoustically-received shift,
though.)     
◦ TRN RESET (Resets the TRN particle ﬁlter to the default internally). reinit     
◦ TRN RESET xxxx yyyy zzzz s
(Takes 3 or 4 parameters. xxx=x offset. yyy=y offset. zzz=z offset. s=swap +- signs of previous params.1 will swap signs, 0 will not). S is optional arg, if not sending, will default to not swap.
It looks like swap signs is the correct param from a one time test with MBARI. pass 1 as last argument.
◦ TRN RESTORE (Restores the previous MC shift that sentry operators sent
acoustically.) This restore will only persist if forwarding is disabled.     
◦ TRN FORWARD_ENABLE (The second command needed to enable forwarding
TRN shifts to MC and it allows restoration of the acoustic MC shift after the
TRN ENABLE period has timed out.)     
◦ TRN FORWARD_DISABLE (Disable TRN shift forwarding to MC. The node will
still transmit TRN shift information to the acomms queue, though.)     

SDQ 100 :    0 1 2 3 4 5 6 7 8 9 10     
    | | | | | | | | | | |     
    x x x x x x x x x x MCA SHFTABS x x     
    0) Last watchstander shift (easting)     
    1) Last watchstander shift (northing)     
    2) System armed (Forwarding enabled)     
    3) System active (TRN enabled)     
    4) Remaining active system timeout, in seconds    
    5) Last TRN shift estimate (-northing).     
    6) Last TRN shift estimate (-easting) 5 + 6 may show lat lon instead of shift if no good soundings are available.    
    7) Current TRN solution is valid (Will show 1 if yes). Means the server filter has converged on a solution    
    8) Last valid TRN shift estimate (-northing)    
    9) Last valid TRN shift estimate (-easting)    
    10) Actual shift command sent from the system to the mission (if system is armed)    
    
-----------------------
    
    
## Usage Tip
This software needs to work while disconnected from a network. A simple way to check functionality, is  to disconnect the vehicle from the nwtwork with
node running, the kongsberg pinging and trn update messages broadcasting.     
Upon reconnect, if the node  was still receiving mb state updates from the datapod, you will know things are configured right.           

There are slight differences, but in actual usage, you will most likely want to always enable trn, and enable forwarding for things to work as expected
 to have mc receive shifts from trn to the vehicle.      

Listen to the Trn Messages! "rostopic echo /sentry/nav/ds_trn_cli/TrnUpdate"     

## Sentry checklist.
Step by step instructions for running trn have been added to the sentry checklist on 2022-nooner.     

## Example in order  acomms communication similar to what was used during a dive  through SMS 2022-nooner
* Depends on objectives

SDQ 100    

SDQ 100    
 
UDP0 TRN ENABLE 90000 1 0         (Sets the enable timeout for 25 hours. run all mission!)     
SDQ 100     
UPD0 TRN FORWARD_ENABLE     
SDQ 100    

UDP0 TRN RESET    

SDQ 100    

UDP0 TRN DISABLE    
UDP0 TRN FORWARD_DISABLE    
SDQ 100    

UDP0 TRN RESTORE    
SDQ 100    

-----------------------
End of Dive     


   
