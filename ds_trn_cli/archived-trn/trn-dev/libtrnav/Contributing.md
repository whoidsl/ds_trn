
# Contributing

When contributing to this repository, please first discuss the change you wish to make via issue, email, or any other method with the dev team.

*The gatekeeper for this repository is Kent Headley : headley@mbari.org x1822.*

## Branching model

We've adopted a branching strategy to ensure that libtrnav works consistently across multiple platforms:  
    - Dorado AUV  
    - LRAUV  
    - MB-System
    - Sentry . 

## Terms
Pull Request (PR)  
Code Review  (CR)  

### Developing new code

1. *Master* is the production branch, so no direct commits allowed.
2. You should develop new code under a *feature branch* (typically branched from master)
    * Each feature should have a dedicated branch
    * **Keep your feature branch up to date with Master to avoid merge conflicts**
3. Once you're done developing your new feature and have tested it using sim/lab/test-tank/deployment, create a pull-request (PR) to a *release* branch (and schedule a code review (CR))
4. Once approved, the feature branch will be merged to the release branch and
   closed. Release branches will be tested at sea (possibly for several
   deployments) and may include more than one feature
5. Push bug fixes and updates to the release branch
6. Finally, merge the release branch to Master via pull request

Example:

            git checkout master
            git pull [see note 1]
            git checkout -b feature/2019-04-29-cool-new-feature
            # develop on your feature branch...

            *--A---B----------------------C--  master
                    \                    /
                     \            *--R--S    release
                      \             /
                       N---O---P---Q    cool feature

            # done? create PR to release and schedule a CR
            [1] Note: pull --rebase changes your local branch history. 
                  git checkout master
                  git pull
                will use merge, which preserves branch history
                
Remember to keep your feature branch up to date with Master!

Example -

You are working on a feature:

            *--A---B---C---D---E--- master
                \
                 N---O---P---Q----- feature

            # use
            git fetch origin # update origin/master from the server
            git stash # if you have uncommitted local changes

            # then use
            git checkout master # check out your local tracking branch ...
            git pull # ... and bring it up to date
            git checkout feature/2019-04-29-cool-new-feature # go back to your feature branch
            git merge master # do the actual merge to obtain:

             *--A---B---C---D---E--- master
                 \               \
                  N---O---P---Q---R-- feature

            # after merging, remember to restore uncommitted changes:
            git stash pop

### Fixing/updating existing code

1. Create a bugfix/hotfix branch and commit your changes to it. Each fix should have a dedicated branch
2. Test using sim / lab / test tank / deployment!
3. Create a pull request to Master
4. Propagate changes to release and feature branches

Example:

            git checkout master
            git pull
            git checkout -b bugfix/2019-04-29-fix-nasty-bug
            # commit changes to your fix branch...

             *--A---B-----C--  master
                     \    /\
              bugfix  X--Y  \
                             \
                  N---O---P---Q some release

            # done? create a PR to Master and merge to release/feature

Note about hotfixes: please consider creating a feature branch depending on the scope of the changes you're introducing.

### Best Practices for solving cross-platform issues
    [TBD]

## Code of conduct

1. Never rebase a public branch (i.e. one that anyone else is working on)
       If you're not certain what that means, don't use rebase.

2. Never commit code that breaks the build 

        # run the static analyzer and fix any issues
        cppcheck --enable=all --std=c99 --force --quiet <dir>...
        # enable all compiler warnings and error checking, address all warnings or errors
        # please test your changes by running the project's unit/regression tests
        [procedure TBD]
        # build and test for all platforms that will run the code

3. Make sure your code meets the project's style guidelines before committing

        # keeping your code in style is easy!
        [procedure TBD], e.g.,
 
        make pretty # to format the code (ansi, aStyle 2.05.1)
        make cxxcheck # ... to check Cxx style

4. If you're making changes in a sub-repo make sure sub-repo pointers point to the right commit

        [does not currently apply to libtrnav]
        # test using:
        cd path/to/libtrnav
        git submodule update --recursive
        # verify the intended commits are checked out in the sub-repos

### Git branch naming

1. Feature branch: `feature/yyyymmdd-feature-desribe-feature`
2. Bugfix branch : `bugfix/yyyymmdd-fix-describe-bug`
3. Hotfix branch : `hotfix/yyyymmdd-hot-describe-fix`
3. Release branch: `release/yyyymmdd-libtrnav`

### Git commit messages

1. Commit messages should succinctly describe the changes you're introducing
2. Use the present tense ("Add feature" not "Added feature")
3. Use the imperative mood ("Move cursor to..." not "Moves cursor to...")
4. Limit the first line to 72 characters or less

[adapted from LRAUV Contributing.md by Ben Raanan]