#!/usr/bin/bash
#
usage() {
  echo "Usage: win-proc-resource-mon.sh  MON-NAME [INTERVAL]"
  echo "  MON-NAME.txt is the input file containing the resource categories to"
  echo "  monitor and MON-NAME-stats-YYYY-DAY-HHMMSS.csv is the output file."
  echo "  Leave out the \".txt\" of the file name in the argument (see example)."
  echo "  INTERVAL is the optional interval in seconds between resource"
  echo "  samples. Default interval is 2 seconds."
  echo ""
  echo "Example:"
  echo "  \$ win-proc-resource-mon.sh trn_server"
  echo "    - looks for input file \"trn_server.txt\" in local directory and creates"
  echo "    - file \"trn_server-stats-2019-050-092707.csv\" in local directory."
}

# check for an argument
#
if [ $# -eq 0 ]; then
  echo "Error: win-proc-resource-mon.sh requires at least one argument."
  usage
  exit
fi

INTERVAL=2     # every two seconds
POSTFIX=`date "+%Y-%j-%k%M%S"`
CSVFILE=$1-stats-$POSTFIX.csv
TXTFILE=$1.txt

# check for the input file
#
if [ ! -e $TXTFILE ]; then
  echo "Error: $TXTFILE not found."
  usage
  exit
fi

# ensure that the output file does not already exist, although this is unlikely
#
if [ -e $CSVFILE ]; then
  echo "Error: $CSVFILE already exists (strange)."
  usage
  exit
fi

# go for it
#
echo "   Using outline in $TXTFILE..."
echo "   Writing resource usage to $CSVFILE..."
if [ -n "$2" ] ; then
  INTERVAL=`expr $2 + 0`
  echo "   Using sampling interval of $INTERVAL seconds..."
fi
echo "   typeperf -cf $1.txt -si $INTERVAL -f CSV -o $1-stats-$POSTFIX.csv"
echo "   Ctrl-C to quit"

typeperf -cf $1.txt -si $INTERVAL -f CSV -o $1-stats-$POSTFIX.csv
