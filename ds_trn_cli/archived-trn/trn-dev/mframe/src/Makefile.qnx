#############################################################################
# build environment

# version and build configuration

# uncomment and optionally define on command line
# to override value defined in source, e.g.
#   MF_VER=1.0.1 make
# MF_VER ?=1.0.0
MF_BUILD ?=`date +%Y/%m/%dT%H:%M:%S%z`
#LIB_VER=-DMFRAME_VER=$(MF_VER)
LIB_BUILD=-DMFRAME_BUILD=$(MF_BUILD)
BUILD_OPTS=

# build mstats unit test
# [use WITH_MSTATS_TEST=1 make...]
ifdef WITH_MSTATS_TEST
BUILD_OPTS+= -DWITH_MSTATS_TEST
endif

# include medebug optional macros
# [use WITH_MEDEBUG_OPTIONAL=1 make...]
ifdef WITH_MEDEBUG_OPTIONAL
BUILD_OPTS+= -DWITH_MEDEBUG_OPTIONAL
endif

# include medebug debug macros
# [use WITH_MEDEBUG_DEBUG=1 make...]
ifdef WITH_MEDEBUG_DEBUG
BUILD_OPTS+= -DWITH_MEDEBUG_DEBUG
endif

# include medebug required macros
# [use WITHOUT_MEDEBUG_REQUIRED=1 make...]
ifdef WITHOUT_MEDEBUG_REQUIRED
BUILD_OPTS+= -DWITHOUT_MEDEBUG_REQUIRED
endif

# build mmdebug disable
# [use WITHOUT_MMDEBUG=1 make...]
ifdef WITHOUT_MMDEBUG
BUILD_OPTS+= -DWITHOUT_MMDEBUG
endif

# build mmdebug unit test
# [use WITH_MMDEBUG_TEST=1 make...]
ifdef WITH_MMDEBUG_TEST
BUILD_OPTS+= -DWITH_MMDEBUG_TEST
endif

# include mmdebug unit test optional macros
# [use WITH_MMT_OPTIONAL=1 make...]
ifdef WITH_MMT_OPTIONAL
BUILD_OPTS+= -DWITH_MMT_OPTIONAL
endif

# include mmdebug unit test debug macros
# [use WITH_MMT_DEBUG=1 make...]
ifdef WITH_MMT_DEBUG
BUILD_OPTS+= -DWITH_MMT_DEBUG
endif

# exclude mmdebug unit test required macros
# [use WITHOUT_MMT_REQUIRED=1 make...]
ifdef WITHOUT_MMT_REQUIRED
BUILD_OPTS+= -DWITHOUT_MMT_REQUIRED
endif

# build msocket unit test
# [use WITH_MSOCKET_TEST=1 make...]
ifdef WITH_MSOCKET_TEST
BUILD_OPTS+= -DWITH_MSOCKET_TEST
endif

# build mserial unit test
# [use WITH_MSERIAL_TEST=1 make...]
ifdef WITH_MSERIAL_TEST
BUILD_OPTS+= -DWITH_MSERIAL_TEST
endif

# build mstats unit test
# [use WITH_MSTATS_TEST=1 make...]
ifdef WITH_MSTATS_TEST
BUILD_OPTS+= -DWITH_MSTATS_TEST -DMST_STATS_EN
endif

# build medebug unit test
# [use WITH_MEDEBUG_TEST=1 make...]
ifdef WITH_MEDEBUG_TEST
BUILD_OPTS+= -DWITH_MEDEBUG_TEST
endif

# build all unit tests
# [use WITH_TESTS=1 make...]
ifdef WITH_TESTS
BUILD_OPTS+= -DWITH_MST_MSTATS -DWITH_MMDEBUG -DWITH_MEDEBUG_TEST -DWITH_MEDEBUG_DEBUG -DWITH_MEDEBUG_OPTIONAL -DWITH_MSOCKET_TEST -DWITH_MSERIAL_TEST -DWITH_MMDEBUG_TEST -DWITH_MSTATS_TEST -DMST_STATS_EN
endif

# user symbols
# C standard e.g. -std=c99 -std=gnu99
# may be needed for Cygwin (e.g. for loop declare/init)
#STD= -std=c99
#OPTIONS= $(STD) -DMD_OLEVEL=MD_DEBUG -D_GNU_SOURCE $(BUILD_OPTS) $(LIB_VER) $(LIB_BUILD) 
OPTIONS= $(STD) -D_GNU_SOURCE $(BUILD_OPTS)

# dependency paths

# build directories
OUTPUT_DIR=../bin
BUILD_DIR=../build

# compilation
#CXX = gcc
#AR = ar
CXX = cc
AR = wlib
DEPEND_TOOL = makedepend

# compile and link with -pg for profiling support
# then do gprof <application> gmon.out for profile output to console
GPROF= #-pg # for gprofile support
#CFLAGS = -g -Wall $(GPROF) -Wno-deprecated -Wsign-compare -O2 $(OPTIONS) 
CFLAGS = $(OPTIONS)  #-D_QNX -DUNIX  -WC,-ei -WC,-xs  
LD_FLAGS = $(GPROF) -g #-lm
QNX_PATHS = -I/usr/include
INC_PATHS =  -I. $(QNX_PATHS)
LIB_PATHS = -L$(OUTPUT_DIR)
AR_OPTS =

#############################################################################
# target definitions

# libmframe : main target
LIBMFRAME=libmframe.a
LIBMFRAME_SRC=mframe.c mbbuf.c mcbuf.c mlist.c merror.c  \
mlog.c mstats.c mlist.c mmdebug.c mutils.c mtime.c \
mfile.c mserial.c mthread.c mmem.c mqueue.c msocket.c
#mdebug.c mconfig.c mhash.c mtree.h

LIBMFRAME_OBJ=$(LIBMFRAME_SRC:%.c=$(BUILD_DIR)/%.o)

# mframe-test : test libmframe
MFRAME_TEST=mframe-test
MFRAME_TEST_SRC=mframe-test.c 
MFRAME_TEST_OBJ=$(MFRAME_TEST_SRC:%.c=$(BUILD_DIR)/%.o)
MFRAME_TEST_LIBS = -lmframe #-lpthread
MFRAME_TEST_CFLAGS = -DWITH_MSOCKET_TEST

MSTATS_TEST=mstats-test
MSTATS_TEST_SRC=mstats-test.c
MSTATS_TEST_OBJ=$(MSTATS_TEST_SRC:%.c=$(BUILD_DIR)/%.o)
MSTATS_TEST_LIBS = -lmframe -lpthread

MMDEBUG_TEST=mmdebug-test
MMDEBUG_TEST_SRC=mmdebug-test.c
MMDEBUG_TEST_OBJ=$(MMDEBUG_TEST_SRC:%.c=$(BUILD_DIR)/%.o)
MMDEBUG_TEST_LIBS = -lmframe -lpthread

MEDEBUG_TEST=medebug-test
MEDEBUG_TEST_SRC=medebug-test.c
MEDEBUG_TEST_OBJ=$(MEDEBUG_TEST_SRC:%.c=$(BUILD_DIR)/%.o)
MEDEBUG_TEST_LIBS = -lmframe -lpthread

MSOCKET_TEST=msocket-test
MSOCKET_TEST_SRC=msocket-test.c
MSOCKET_TEST_OBJ=$(MSOCKET_TEST_SRC:%.c=$(BUILD_DIR)/%.o)
MSOCKET_TEST_LIBS = -lmframe -lpthread

# doxyen: Doxygen documentation

#############################################################################
# build files (mostly for cleanup)
SOURCES = $(LIBMFRAME_SRC) $(MFRAME_TEST_SRC)
#$(MFRAME_TEST_SRC) $(LIBMFRAME_SRC) $(MSTATS_TEST_SRC) $(MMDEBUG_TEST_SRC) $(MEDEBUG_TEST_SRC) $(MSOCKET_TEST_SRC)
OBJECTS = $(SOURCES:%.c=$(BUILD_DIR)/%.o) $(OUTPUT_DIR)/$(LIBMFRAME)
DEPENDS = $(SOURCES:%.c=$(BUILD_DIR)/%.d)
BINARIES = $(OUTPUT_DIR)/$(MFRAME_TEST) $(OUTPUT_DIR)/$(MSTATS_TEST) $(OUTPUT_DIR)/$(MMDEBUG_TEST) $(OUTPUT_DIR)/$(MEDEBUG_TEST) $(OUTPUT_DIR)/$(MSOCKET_TEST)
DSYMS   = $(BINARIES:%=%.dSYM)
CLEANUP = gmon.out

#############################################################################
# rules: build targets

#all: $(OBJECTS) $(OUTPUT_DIR)/$(LIBMFRAME) $(OUTPUT_DIR)/$(MFRAME_TEST) $(OUTPUT_DIR)/$(MSTATS_TEST) $(OUTPUT_DIR)/$(MMDEBUG_TEST) $(OUTPUT_DIR)/$(MEDEBUG_TEST) $(OUTPUT_DIR)/$(MSOCKET_TEST)
all: $(OBJECTS) $(OUTPUT_DIR)/$(LIBMFRAME) $(OUTPUT_DIR)/$(MFRAME_TEST) $(OUTPUT_DIR)/$(MSTATS_TEST) 
# $(OUTPUT_DIR)/$(MFRAME_TEST) $(OUTPUT_DIR)/$(MMDEBUG_TEST) $(OUTPUT_DIR)/$(MEDEBUG_TEST) $(OUTPUT_DIR)/$(MSOCKET_TEST)

# build mframe library
$(OUTPUT_DIR)/$(LIBMFRAME):	$(LIBMFRAME_OBJ)
	@echo building $@...
	$(AR) $(AR_OPTS) $@ +- $^

#$(LIBMFRAME_OBJ)
#    $(AR) -r $@ $(LIBMFRAME_OBJ)

# build mframe_test utility
$(OUTPUT_DIR)/$(MFRAME_TEST): $(MFRAME_TEST_OBJ) $(OUTPUT_DIR)/$(LIBMFRAME)
	@echo building $@...
	$(CXX) $(INC_PATHS) $(OPTIONS) $(MFRAME_TEST_CFLAGS) $(LIB_PATHS) $^ -o $@ $(MFRAME_TEST_LIBS) $(LD_FLAGS)
	@echo

# build mstats_test utility
$(OUTPUT_DIR)/$(MSTATS_TEST): $(MSTATS_TEST_OBJ) $(OUTPUT_DIR)/$(LIBMFRAME)
	@echo building $@...
	$(CXX) $(INC_PATHS) $(OPTIONS) $(LIB_PATHS) $^ -o $@ $(MSTATS_TEST_LIBS) $(LD_FLAGS)
	@echo

# build mmdebug-test utility
$(OUTPUT_DIR)/$(MMDEBUG_TEST): $(MMDEBUG_TEST_OBJ) $(OUTPUT_DIR)/$(LIBMFRAME)
	@echo building $@...
	$(CXX) $(INC_PATHS) $(OPTIONS) $(LIB_PATHS) $^ -o $@ $(MMDEBUG_TEST_LIBS) $(LD_FLAGS)
	@echo

# build medebug-test utility
$(OUTPUT_DIR)/$(MEDEBUG_TEST): $(MEDEBUG_TEST_OBJ) $(OUTPUT_DIR)/$(LIBMFRAME)
	@echo building $@...
	$(CXX) $(INC_PATHS) $(OPTIONS) $(LIB_PATHS) $^ -o $@ $(MEDEBUG_TEST_LIBS) $(LD_FLAGS)
	@echo

# build msocket-test utility
$(OUTPUT_DIR)/$(MSOCKET_TEST): $(MSOCKET_TEST_OBJ) $(OUTPUT_DIR)/$(LIBMFRAME)
	@echo building $@...
	$(CXX) $(INC_PATHS) $(OPTIONS) $(LIB_PATHS) $^ -o $@ $(MSOCKET_TEST_LIBS) $(LD_FLAGS)
	@echo

#-include $(DEPENDS)
include $(DEPENDS)

# rule: build object files from source files
$(BUILD_DIR)/%.o :%.c 
	@echo compiling $<...
	$(CXX) $(CFLAGS) $(INC_PATHS) -c $< -o $@
	@echo

$(BUILD_DIR)/%.d :%.c
	@echo Generating dependencies for $?
	@$(DEPEND_TOOL) $(INC_PATHS) $? -f $@

# rule: build dependency files from source files
#$(OUTPUT_DIR)/%.d :%.c
#	@[ -d $(OUTPUT_DIR) ] || mkdir -p $(OUTPUT_DIR)
#	@echo generating dependency file for $<
#	@$(DEPEND_TOOL) $(INC_PATHS) $? -f $@.tmp
#	@mv $@.tmp $@
#	@echo

#$(BUILD_DIR)/%.d :%.c
#	@[ -d $(BUILD_DIR) ] || mkdir -p $(BUILD_DIR)
#	@[ -d $(OUTPUT_DIR) ] || mkdir -p $(OUTPUT_DIR)
#	@echo generating dependency file for $<
#	@set -e; $(CXX) -MM $(CFLAGS) $(INC_PATHS) $< \
#	| awk '/o:/ {printf "%s", "$@ $(BUILD_DIR)/"} {print}' > $@; \
#	[ -s $@ ] || rm -f $@
#	@echo

install:
	@echo "Installing...(not implemented)"


###########################################################################
# rules:

.PHONY: clean
.PHONY: purge

.SUFFIXES:
.SUFFIXES:    .o .c .cc .d .idl


# clean : delete object, dependency, binary files
clean:
	rm -f $(OBJECTS) $(DEPENDS) $(BINARIES)
	rm -rf $(DSYMS)

# purge : delete delete object, dependency, binary files, build directories
purge:
	rm -f $(BINARIES) $(OBJECTS) $(DEPENDS) $(DSYMS) $(CLEANUP)
	rm -rf $(OUTPUT_DIR) $(BUILD_DIR)

# include the dependencies
ifneq ($(MAKECMDGOALS),purge)
ifneq ($(MAKECMDGOALS),clean)

endif
endif
