#!/bin/bash

OPTS="\
-v \
--leak-check=full \
--show-reachable=yes \
--track-origins=yes \
--leak-resolution=high \
--num-callers=40"
#--suppressions=path/to/valgrind.supp
#--gen-suppressions=yes

echo valgrind $OPTS $*
valgrind $OPTS $*

