# mframe

A set of templates and components for writing C libraries, applications, scripts and makefiles.

# Building mframe

Download mframe

	git clone git@bitbucket.org:klheadley/mframe.git
        
Build is controlled by a single makefile; autotools is not used for simplicity, though it is easy to integrate mframe with autotools (an example Makefile.am is included in the templates directory)

    cd mframe
    [<VAR>=1...] make clean all
   
   Example: build with unit tests
       WITH_TESTS=1 make clean all

   Example: build with medbug pdebug, debug and optional enabled
       WITH_PDEBUG=1 WITH_MEDEBUG_OPTIONAL make clean all
    
## Build environment variables
    WITH_MST_MSTATS        : build with mstats macros
    MST_STATS_EN           : enable mstats macros
    WITH_PDEBUG            : enable medebug macros
    WITH_MMDEBUG           : build with mmdebug macros

    WITH_MEDEBUG_DEBUG     : enable medebug debug macros
    WITH_MEDEBUG_OPTIONAL  : enable medebug optional macros

    WITH_TESTS             : enable all unit tests
    WITH_MSOCKET_TEST      : build msocket unit test
    WITH_MSERIAL_TEST      : build mserial unit test
    WITH_MSTATS_TEST       : build mstats unit test
    WITH_MEDEBUG_TEST      : build medebug unit test  
    WITH_MMDEBUG_TEST      : build mmdebug unit test
    WITH_MSTATS_TEST       : build mstats unit test
    WITH_MMT_DEBUG         : enable mmdebug unit test debug 
    WITHOUT_MMT_REQUIRED   : disable mmdebug unit test required 

# Using mmdebug
The mmdebug module provides module-specific debug output control that is configurable at run time.
Configuration is done by defining (enum) IDs for modules and channels; this may be done in each module, but it is recommended to centralize these definitions when multiple modules are being used. mframe provides the files mconfig.h and mconfig.c for this purpose. 
    
    * in configuration header (e.g. mconfig.h):
        - module ID enum
        - channel ID enum (per module)
        - channel bit masks (per module)
        - include mmdebug.h

    * in configuration code (e.g. mconfig.c:mconf_init()):
        - define channel names (optional)
        - define default module configuration(s)
        
    * in module (application) code:
        - call mconf_init
        - use mmdebug macros
        - use mmdebug API functions to change configuration at run time
    

## Example config

### mconfig.h

    /// @enum app_module_ids
    /// @brief module IDs.
    /// Must be unique across all application modules.
    /// IDs 0-MM_MODULE_COUNT are reserved for mframe
	typedef enum{
        MOD_FOO=MM_MODULE_COUNT,
        MOD_FOO,
        MOD_BAR,
        APP_MODULE_COUNT
    }app_module_ids;

    /// @enum foo_channel_id
    /// @brief module channel IDs.
    /// These enum values represent bit index in the channel mask.
    /// Channels 0-MM_CHANNEL_COUNT are reserved for mframe 
    /// channels, e.g. MM_DEBUG, MM_WARN, MM_ERR, etc.
    /// Applications are not required to use these
    typedef enum{
        ID_FOO_V1=MM_CHANNEL_COUNT,
        ID_FOO_V2,
        FOO_CH_COUNT
    }foo_channel_id;

    /// @enum foo_channel_mask
    /// @brief module channel bit masks
    /// Uses channel ID to index
    typedef enum{
        FOO_V1= (1<<ID_FOO_V1),
        FOO_V2= (1<<ID_FOO_V2)
    }foo_channel_mask;

    /// @enum bar_channel_id
    /// @brief module channel IDs
    typedef enum{
        ID_BAR_V1=MM_CHANNEL_COUNT,
        ID_BAR_V2,
        ID_BAR_PARSER,
        ID_BAR_DRFCON,
        BAR_CH_COUNT
    }bar_channel_id;

    /// @enum bar_channel_mask
    /// @brief module channel bit masks
    typedef enum{
        BAR_V1= (1<<ID_BAR_V1),
        BAR_V2= (1<<ID_BAR_V2),
        BAR_PARSER= (1<<ID_BAR_PARSER),
        BAR_DRFCON= (1<<ID_BAR_DRFCON)
    }bar_channel_mask;

### mconfig.c

Defining channel names is optional; is unused, the channel name array may be set to NULL in the configuration.
When a module extends the built-in channels (trace, debug, warn, err),
 the channel name definitions must include names for these channels. 

    /// @var char *foo_ch_names[FOO_CH_COUNT]
    /// @brief module channel names
    char *foo_ch_names[FOO_CH_COUNT]={
        "trace.foo",
        "debug.foo",
        "warn.foo",
        "err.foo",
        "foo.v1",
        "foo.v2"
    };

    /// @var char *bar_ch_names[APP_CH_COUNT]
    /// @brief module channel names
    char *bar_ch_names[BAR_CH_COUNT]={
        "trace.bar",
        "debug.bar",
        "warn.bar",
        "err.bar",
        "bar.v1",
        "bar.v2",
        "bar.parser",
        "bar.drfcon"
    };

Module debug configuration defaults may be defined individually or in an array (which simplifies initialization )

    /// @var mmd_module_config_t mmd_config_defaults[]
    /// @brief module configuration defaults
    static mmd_module_config_t mmd_config_defaults[]={
        {MOD_FOO,"MOD_FOO",FOO_CH_COUNT,((MM_ERR|MM_WARN)|FOO_V1),foo_ch_names},
        {MOD_BAR,"MOD_BAR",BAR_CH_COUNT,((MM_ERR|MM_WARN)|BAR_V1),bar_ch_names}
    };

Add configuration code to mconf_init, typically called when the application starts

    /// @fn void mcfg_init()
    /// @brief Application specific module configuration.
    /// @return none
    int mconf_init(void *pargs, void *prtn)
    {
        int retval=-1;
        // User code....
        
        // call mmd_module_configure() for each module
        int i=0;
        int app_modules =(APP_MODULE_COUNT-MM_MODULE_COUNT);
        for(i=0;i<app_modules;i++){
        	mmd_module_configure(&mmd_config_defaults[i]);
        }
        return retval;
    }

### application
    #include "mmdebug.h"
    #include "mconfig.h"

    int main(){
        mconf_init();
        
        PMPRINT(MOD_FOO,MM_WARN,("MM_WARN\n"));       
        PMPRINT(MOD_FOO,MM_ERR|MM_DEBUG,("MM_ERR | MM_DEBUG\n"));

        mmd_channel_en(MOD_FOO,MM_DEBUG);
        PMPRINT(MOD_FOO,FOO_V1|MM_DEBUG,("FOO_V1 | MM_DEBUG\n"));       
        mmd_channel_en(MOD_FOO,FOO_V1);
        PMPRINT(MOD_FOO,FOO_V1,("FOO_V1 (en)\n")); 
        mmd_channel_dis(MOD_FOO,FOO_V1);
        PMPRINT(MOD_FOO,FOO_V1,("FOO_V1 (dis)\n")); 
          
        PMPRINT(MOD_BAR,BAR_PARSER,("BAR_PARSER (dis - should not print)\n"));   
        mmd_channel_en(MOD_BAR,BAR_PARSER);    
        PMPRINT(MOD_BAR,BAR_PARSER,("BAR_PARSER (en)\n"));   
    }

