#include <cstdlib>

#ifdef __cplusplus
extern "C" {
#endif

#include "trnu_cli.h"
#ifdef __cplusplus
}
#endif // cplusplus

class TrnuCli{
public:
    TrnuCli(char *host, int port=8000, double hbeat_to_sec=0.0, double recon_to_sec=3.0);
    ~TrnuCli();
    void start();
    void stop();

    int cpp_update_cb(trnu_pub_t *update){
        int retval=0;
        trnucli_ctx_t *ctx = TrnuCli::g_cli_ref->_ctx;
        fprintf(stderr,"ctx [%p] got an update[%p]\n",ctx,update);

        trnucli_ctx_show(ctx,true, 5);
        trnu_pub_t latest={0};
        if(trnucli_ctx_last_update(ctx,&latest,NULL)==0){
            // format per config (pretty, hex, csv, etc.)
            char *str=NULL;
            trnucli_update_str(update,&str,0,TRNUC_FMT_PRETTY);
            if(NULL!=str){
                fprintf(stderr,"%s\n",str);
                free(str);
            }
            str=NULL;
        }

        return retval;
    }
    static int c_update_cb(trnu_pub_t *update){
        int retval=g_cli_ref->cpp_update_cb(update);
        return retval;
    }

    static void SetRef(TrnuCli *ref){
        g_cli_ref=ref;
    };

    static TrnuCli *GetRef(){
        return g_cli_ref;
    };

private:
    trnucli_ctx_t *_ctx;
    char *trnu_host;
    int trnu_port;
    double hbeat_to_sec;
    double recon_to_sec;
    static TrnuCli *g_cli_ref;
};
