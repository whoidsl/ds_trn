
#include "TrnuCli.h"

TrnuCli *TrnuCli::g_cli_ref=NULL;


TrnuCli::TrnuCli(char *host, int port, double hbtos, double rctos)
{
    _ctx = trnucli_ctx_new_dfl(host,port,
                               &TrnuCli::c_update_cb,
                               hbtos,
                               rctos);

    g_cli_ref=this;
    trnu_host=strdup(host);
    trnu_port=port;
    hbeat_to_sec=hbtos;
    recon_to_sec=rctos;
}
TrnuCli::~TrnuCli()
{
    trnucli_ctx_destroy(&_ctx);

}
void TrnuCli::start()
{
    trnucli_ctx_start(_ctx);
}
void TrnuCli::stop()
{
    trnucli_ctx_stop(_ctx);
}


