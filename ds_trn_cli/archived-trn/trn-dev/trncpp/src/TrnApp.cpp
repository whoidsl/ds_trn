#include <cstdio>
#include <cstdlib>
#include <signal.h>
#include <unistd.h>

#include "TrnuCli.h"

#ifdef __cplusplus
extern "C" {
#endif
#include "trnu_cli.h"
#include "trnw.h"

    static bool g_interrupt=false;

#ifdef __cplusplus
}
#endif // cplusplus

static bool g_signal=0;

/// @fn void termination_handler (int signum)
/// @brief termination signal handler.
/// @param[in] signum signal number
/// @return none
static void s_termination_handler (int signum)
{
    switch (signum) {
        case SIGINT:
        case SIGHUP:
        case SIGTERM:
            fprintf(stderr,"INFO - sig received[%d]\n",signum);
            g_interrupt=true;
            g_signal=signum;
            break;
        default:
            fprintf(stderr,"ERR - s_termination_handler: sig not handled[%d]\n",signum);
            break;
    }
}
// End function termination_handler

int main(int argc, char **argv)
{
    // configure signal handling
    // for main thread
//    struct sigaction saStruct;
//    sigemptyset(&saStruct.sa_mask);
//    saStruct.sa_flags = 0;
//    saStruct.sa_handler = s_termination_handler;
//    sigaction(SIGINT, &saStruct, NULL);
    signal(SIGINT, s_termination_handler);

    int retval=-1;
    char host[80]="localhost";
    TrnuCli tcli(host,8000,8.0,5.0);
    tcli.start();
    while(!g_interrupt){
        sleep(3);
    }
    tcli.stop();
    return retval;
}
